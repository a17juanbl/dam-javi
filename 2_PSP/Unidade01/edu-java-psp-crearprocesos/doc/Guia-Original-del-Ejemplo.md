# Implementación del ejemplo CrearProcesos

## Pasos generales a seguir 

Vamos a ver cómo implementar una aplicación que va lanzando procesos de otra aplicación.

Los pasos generales que vamos a seguir son:

1. Crear y generar la aplicación de la que lanzaremos sus procesos.
2. Crear un nuevo proyecto que será el creador de procesos.
3. Implementar el código necesario para crear procesos.
4. Probar la ejecución del creador de procesos.
5. Posibles mejoras

Finalizaremos con unas recomendaciones para probar el ejemplo.



## Crear una aplicación para lanzar sus procesos

Para utilizar una aplicación funcional y no tener que implementarla completa, vamos a utilizar uno de los ejemplos que incorpora Netbeans, "Document Editor".

Puedes utilizar una aplicación que tengas ya generada si lo prefieres. Lo que nos interesa es tener el ejecutable de la aplicación (en caso de Java, su .jar)

Procedemos de la siguiente manera:

1. Abrimos NetBeans
2. Probar un proyecto

	![alt text](img/netbeans.png)

	> Si no aparece la página de inicio en donde ejecutar "Probar un proyecto" ve a `Ayuda --> Página de Inicio`

3. Seleccionamos el proyecto de Ejemplo Java llamado "Document Editor"

	![alt text](img/netbeans2.png)

4. Una vez seleccionado pulsamos el botón siquiente
5. Indicamos el nombre con el que guardaremos el nuevo proyecto y su ubicación. En mi caso, voy a llamar al proyecto "Editor" y lo guardamos en la carpeta que deseemos (aunque yo he elegido la carpeta por defecto)
6. Pulsamos el botón Terminar. Es importante que se establezca como proyecto principal para que genere su *.jar*.

	![alt text](img/netbeans3.png)

7. Generar el proyecto principal. Menú ejecutar --> Generar Main Project (F11) o limpiar y generar main Project (Mayús+F11)

	![alt text](img/netbeans4.png)


## Crear un proyecto, el creador de procesos

Vamos a crear un proyecto que creará procesos de tipo "Editor".
Comencemos:

1. Añadimos un nuevo proyecto. `Menu Archivo --> Proyecto Nuevo... (Ctrl+Mayús+N)`

2. Tipo de proyecto: `Java --> Java Desktop Aplication`

	![alt text](img/netbeans5.png)

3. `Tipo de aplicación --> Aplicación Básica`

4. Indicamos el nombre con el que guardaremos el nuevo proyecto y ubicación.

5. El entorno nos muestra un mensaje en el que nos avisa de que el *Framework Swimg* no va a seguir desarrollándose para formar parte del Kit de desarrollo java (por lo que nuestro proyecto no va a ser todo lo portable que nos gustaría)
6.
6. Indicamos el nombre con el que guardaremos el nuevo proyecto y su ubicación.

	![alt text](img/netbeans6.png)

	En este caso podemos observar que el proyecto se a guardar como `CrearProcesos`, que se va a guardar en la carpeta por defecto de NetBeans y es de tipo Aplicación básica

Este es nuestro proyecto de partida:

![alt text](img/netbeans7.png)

***
## Implementar: Crear procesos

Comenzamos incluyendo un control de tipo botón (jButton):
* Modificamos las propiedades *name* y *text* del botón, para que el aspecto de la interfaz sea coherente.
* Incluímos una etiqueta (jlabel) para que dé más información al usuario.

![alt text](img/netbeans8.png)

1. Vamos al código fuente
Vamos a incluír una acción (@Action) para poder asignarla como acción a realizar cuando el usuario haga click sobre el botón.
2. En este caso vamos a copiar la linea 84 (que es donde se encuentra action), para copiarla más abajo.
3. Cambiamos el nombre de la acción *showAboutBox()* por otro, en este caso *crearNuevoEditor()*

![alt text](img/netbeans9.png)

        Una acción es un método de clase que tiene que ser público, no devuelve ningún valor y no recibe ningún parámetro.

4. Comprobamos que el comando con el que crearemos los procesos funciona:

Windows: ```java -jar C:\Users\usuario\Documents\NetBeansProjects\Editor\dist\DocumentEditor.jar```    
Linux: ```java -jar /home/usuario/NetbeansProjects/Editor/dist/DocumentEditor.jar```

5.Incluímos el código necesario para crear procesos, incluímos la clase *java.lang.Process* y *java.lang.Runtime*

![alt text](img/netbeans10.png)

6. Definimos una variable que referenciará al objeto de tipo *Process* que nos permitirá comunicarnos con el proceso. El método *Runtime.getRuntime.exec(Comando)*, nos devuelve un objeto de tipo *Process* y puede generar excepciones, así que debemos capturarlas.

![alt text](img/netbeans11.png)

7. Volvemos al diseño de nuestra ventana de aplicación o formulario.
8. Seleccionamos el botón y en sus propopiedades seleccionamos *action --> crearNuevoEditor* para que cada vez que se clicke se ejecuten las acciones implementadas.

![alt text](img/netbeans12.png)

9. Menu Ejecutar --> Ejecutar Main Project (F6)

**Linux**
![alt text](img/netbeans13.png)
**Windows**
![alt text](img/netbeans14.png)

## Posibles mejoras

Como cualquier otro, este código es muy mejorable. Aquí incluímos algunas de las posibles mejoras:

1. Crear código que sea independiente de la plataforma

2. Crear estructura, por ejemplo una lista, que nos permita tener referenciados todos los objetos proceso que vayamos creado. En el ejemplo utilizamos siempre la misma variable (nuevoProceso); y además, está definida dentro del método *crearNuevoEditor*; lo que no nos permite comunicarnos con ese proceso desde el resto del código del proyecto; y aún más, sólo nos permitiría comunicarnos con el último creado.

3. Poder crear cualquier tipo de proceso, permitiéndole al usuario introducir el comando de ejecución del proceso.

        Recuerda, esto implica incluír, por ejemplo, un cuadro de texto y chequear que ese comando esté bien construído.
        Nosotros vamos a mostrar a continuación una posible implementación para la primera de las mejoras propuestas.

Posible implementación de la primera mejora "Crear código que sea independiente de la plataforma"
1. Obtenemos el nombre del sistema operativo en el que se está ejecutando la aplicación.
2. Dependiendo del SO lanzamos un comando u otro (las rutas cambian según el SO).

![alt text](img/netbeans15_1.png)

![alt text](img/netbeans15_2.png)

## Recomendaciones para probar el ejmeplo

Si no has ido construyendo paso a paso el ejemplo, puedes probar el ejemplo que te suministramos implementado en la plataforma. pero debes tener en cuenta que el código no está preparado para que lo ejecute un usuario con cualquier nombre de usuario. Deberás modificar un poco el código del proyecto *Crearprocesos* antes de lanzar su ejecución.

1. Cambia la ruta de acceso al *.jar*, el nombre de "usuario", por tu nombre de usuario en el sistema

	![alt text](img/netbeans16.png)

2. Si no está generado el ejecutable del editor, puedes generarlo haciendo click con el botón secundario del ratón sobre el nombre del proyecto y seleccionando "Generar".

	![alt text](img/netbeans17.png)

## Reconocimiento

Margarita I. Nieto Castillejo