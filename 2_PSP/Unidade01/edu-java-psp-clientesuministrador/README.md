# README

## Para qué es este repo?

Vamos a crear dos aplicaciones para comunicar un proceso suministrador y otro cliente que utilizaran un fichero a modo de buffer:

1. **Suministrador**, accede al fichero para escribir un dato.

	- Identificaremos las instrucciones que formarán la sección o secciones críticas.

	- Incluiremos mecanismos de sincronización que en este caso, seran escrituras bloqueantes en fichero y bloqueos.

2. **Cliente**, accede en modo escritura a un fichero, para extraer los datos que contenga.

	- Identificaremos las instrucciones que formaran la sección o secciones críticas.
	
	- Incluiremos mecanismos de sincronización que en este caso, serán accesos bloqueantes a fichero y bloqueos.


## Suministrador: Proporcionar datos

* En este caso, vamos a recordar como implementar una aplicación sencilla, que:

	1. Comprueba si el fichero esta vacio.
	
	2. Si está vacio, escribe un dato en el fichero.
	
	3. Si no esta vacio, espera un tiempo y vuelve a comprobar.

* Por supuesto, **gestionaremos las posibles excepciones**, que se pueden producir en los accesos a un fichero.

* **Comprobaremos que la ejecución** de la aplicacion cumple los resultados que esperamos de ella; y que por lo tanto esta **correcta**.


* ¿Que instrucciones de nuestra aplicacion deben ejecutarse como una unidad indivisible?

	1. Comprobar si el fichero está vacio.
	
	2. Escribir el dato.

	Estas van a ser las instrucciones de nuestra sección critica.
	
* Controlaremos que sólo este proceso entrará a ejecutar las instrucciones de su sección crítica. El resto de procesos, encontrarán el recurso bloqueado y esperarán hasta que el que el que consiguió entrar finalice y lo desbloquee. Para ello utilizaremos un objeto de tipo `FileLock`.

* Necesitamos poder abrir el fichero con permisos de lectura y escritura. Utilizaremos un objeto de la clase `RandomAccessFile`. Así no tendremos que utilizar un objeto para leer (`FileReader`) y otro para escribir (`FileWriter`).

* Por supuesto, continuaremos gestionando las posibles excepciones, que se pueden producir en los accesos a fichero.

### Detalle

* Abrimos el fichero en modo `rwd` (lectura-escritura y las escrituras serán llevadas directamente a disco).

* Al abrir el archivo en modo lectura-escritura, sino existe, el sistema operativo lo creará.

* Se resaltan con comentarios las instrucciones que forman la región critica:

	- **Solicitamos el bloqueo** del canal de acceso al fichero.
	
	- Comprobar el tamaño del fichero.
	
	- **Escribir** valor.

	- **Liberar el bloqueo del fichero**.
	
* La solicitud y liberación del bloqueo son las instrucciones que marcan el inicio y el fin de la región critica. De hecho, son las instrucciones **que crean la región critica**.

* La región critica creada, sólo protege al recurso. Solo protege los accesos al fichero y asegura que mientras que este proceso tiene el fichero bloqueado, ningún otro proceso accedera al fichero. Como el bloqueo no se libera entre la comprobación y la escritura, otro proceso no podrá leer el valor hasta que no lo haya escrito este proceso.

* El resto de instrucciones que estan entre el bloqueo y desbloqueo del canal no estan protegidas.

* En la siguientes actividades (Hilos), veremos como crear secciones críticas que protegen todas las instrucciones que las contienen, ya que se crean **a nivel de instrucciones y no a nivel de recurso**.

* Esto sera así, porque los hilos de un mismo proceso pueden **compartir variables** (y zonas de memoria). Las variables no las protege el sistema operativo como sucede con los ficheros. El lenguaje de programación nos proporcionará los mecanismos para crear regiones críticas a nivel de grupo de instrucciones; y todas ellas seran ejecutadas de forma atomica y exclusiva.

* La protección a nivel de recurso proporcionada por el sistema operativo, siempre la tendremos disponible (si el lenguaje y el SO la tienen implementada); tanto para procesos, como para hilos de un proceso.

### Suministrador: Método `FileChannel.lock()`

* El método `lock()` de la clase `FileChannel`, nos devuelve un objeto de
tipo `FileLock` que representa al bloqueo que ha obtenido el proceso sobre el canal de acceso al fichero.

* Para el sistema, el fichero aparecerá como abierto en uso exclusivo para ese proceso.

* Solo podremos solicitar el bloqueo de un fichero, si lo hemos abiertocon permisos de  escritura.

* **El metodo `lock()`, suspende al proceso hasta que este disponible el bloqueo**.

* El método `lock()` puede generar una excepción.

* En el caso de nuestro ejemplo, si no se produce ninguna excepción, el proceso comprobara si hay un dato en el fichero, y si no lo hay, escribira uno. Si sucede alguna excepción, se notificara el error y se continuara intentando el resto de incrementos.

* Debemos darnos cuenta de que la seccion critica, esta definida dentro del bucle y abarca las instrucciones que acceden al recurso compartido. Y, no, esta el bucle completo dentro de la resión critica.


### Suministrador: Método `FileChannel.tryLock()` y la espera activa

* El metodo `FileChannel.tryLock()`, funciona igual que el metodo `lock()`, salvo porque, no bloquea al proceso. Tambien genera excepciones. Por supuesto, siempre es mas recomendable sustituir métodos que puedan generar excepciones por sus equivalentes que no las generen. Pero debemos tener mucho cuidado con no crear en una espera activa que pueda bloquear el sistema.

```
// Ejemplo de espera activa (Un bucle sin instrucciones en su cuerpo, que sólo chequea su condición.)
while((bloqueo = raf.getChannel().tryLock()) != null);
```

* **Espera activa**: se produce cuando un proceso, continuamente testea el estado de un recurso o variable. Lo que hace es consumir tiempo de CPU en una tarea poco productiva.


* **¿Como evitar los problemas que puede provocar?**

	a) **Sustituir la espera activa por métodos bloqueantes equivalentes**. Será el sistema el encargado de suspender el proceso hasta que el recurso este disponible.
	
	b) **Limitar el número de intentos**, para que no sean infinitos. Por ejemplo, cuando intentamos acceder a una pagina web y el servidor esta caldo, nuestro navegador, realiza varios intentos esperando un tiempo entre ellos; despues de ese número de intentos, nos notifica que no ha podido conectar y que podemos intentarlo despues. Nosotros deberemos hacer lo mismo.

* En nuestro caso, como disponemos del metodo `FileChannel.lock()` que suspende al proceso hasta que le sea concedido el bloqueo del fichero (caso A), no nos plantearnos utilizar el metodo `FileChannel.tryLock()`.

* Utilizaremos FileChannel.tryLock() en los casos en los que queramos consultar si un fichero se puede o no bloquear (o si ya esta bloqueado), pero no para obtener el bloqueo del recurso.


### Detalle Suministrador: Método `FileChannel.tryLock()` y la espera activa

* Abrimos el fichero en modo rwd (lectura-escritura y las escrituras seran llevadas directamente a disco).

* Al abrir el archivo en modo lectura-escritura, sino existe, el sistema operativo lo creará.

* Están marcadas con comentarios las instrucciones que forman la region critica:

	- Solicitamos el bloqueo del canal de acceso al fichero.
	
	- Comprobar el tamaño del fichero.
	
	- Leer valor.
	
	- Vaciar el fichero.
	
	- Liberar el bloqueo del fichero.


* La solicitud y liberación del bloqueo son las instrucciones que marcan el inicio y el fin de la region critica. De hecho, son las instrucciones que crean la region critica.


## Cliente - Suministrador: Condiciones de sincronismo

Enfrenta el codigo del Suministrador (izquierda) y el cliente derecha. 

Se puede apreciar, como la condición de sincronismo, es que haya o no dato en el fichero:

* Suministrador no genera (escribe) un nuevo valor hasta que el fichero no esta vacio.

* Cliente no consume (lee y borra) un valor hasta que el fichero no contenga algo.

Debemos tener en cuenta la condición de sincronismo para programar nuestros procesos de forma que ningún proceso quede a la espera de una condición de sincronismo que nunca sucederá.

*Ejemplo*: Si el cliente al leer el valor no lo borra, el suministrador nunca volverá a escribir un valor nuevo. El cliente siempre leerá el mismo
valor. El servidor nunca finalizara su tarea.


## Recomendaciones para probar el ejemplo

- Los comandos seran: java -jar Suministrador nombreArchivoBuffer

- Y este otro: java Cliente nombreArchivobuffer




