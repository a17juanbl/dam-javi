# README

## Para qué es este repo?

Este repo plantea un sencillo ejemplo de acceso a un recurso compartido sin mecanismos de sincronización

Vamos a implementar dos aplicaciones:

Una aplicación que accede a un fichero que contiene un valor entero. Lee ese valor, lo incrementa en 1 y escribe ese valor actualizado en el mismo fichero. Esquemáticamente:

1. `valor = LeerValorFichero(fichero);`
2. `valor++;`
3. `EscribirValorFichero(fichero, valor)`

Una aplicación que crea varios procesos de la anterior. De forma que todos esos procesos utilizarán el mismo fichero para realizar esas senciallas instrucciones.

Veremos cómo probar el ejemplo. ¿El resultado final, es el esperado? ¿Cómo vemos qué es lo que ha pasado?

## Parte 1: Aplicación que modifica el valor de un fichero

En este caso, vamos a recordar como implementar una aplicación sencilla, que:

1. Lee un valor de un fichero.
2. Incrementa ese valor en uno.
3. Escribe ese valor incrementado en el fichero del que lo había leido.

Por supuesto, gestionaremos las posibles excepciones, que se
pueden producir en los accesos a un fichero. Comprobaremos que la ejecución de la aplicación cumple los
resultados que esperamos de ella; y que por lo tanto está
correcta.

Revisa el [código de esta aplicación](src/main/java/psp/parte1/Escritor.java) denominada `Escritor`.

Puedes probarlo directamente con las aplicaciones empaquetadas de la siguiente forma:

```bash
# Sitúate en la raiz del proyecto (donde se encuentra el pom.xml)
java -jar target/escritor
```

## Parte 2: Lanzar la ejecución de varias instancias de la aplicación anterior

Esta segunda aplicación es todavía más sencilla. Simplemente, esta aplicación crea 20 instancias de la primera, de esta forma:

```java
nuevoProceso = Runtime.getRuntime().exec("java -jar " + "escritor " + i + " nuevo.txt");
```

Revisa el [código de esta aplicación](src/main/java/psp/parte2/Lanzador.java) denominada `Lanzador`.

Advierte que intentará lanzar una aplicación que se encuenta en el mismo path desde el que iniciamos esta segunda App que se llama `escritor`.

```bash
# Sitúate en la raiz del proyecto (donde se encuentra el pom.xml)
java -jar target/lanzador
```

## Conclusiones sin mecanismos de sincronización

Como puedes comprobar por ti mismo en la versión de esta práctiva/actividad que está siendo apuntada por la rama `master`, si se lanzan los procesos y la aplicación `escritor` no tiene ningún mecanismo de sincronización para el acceso al recurso compartido, es posible y probable que al lanzar por ejemplo 20 instancias, el número almacenado en el fichero no coincida con 20.

En la versión apuntada por este rama `conSincro` se soluciona ese problema y se logra que la salida del programa en el que colaboras varios procesos dea una salida determinada y esperada.


## Modificaciones para incluir la sincronización

Lo primero a considerar será determinar qué instrucciones de la aplicación `escritor` deben ejecutrase como unidad indivisible.

1. Leer el valor del fichero
2. Incrementar el valor (modificarlo)
3. Escribir nuevamente ese valor en el fichero del que se había leído

**Estas van a ser las instrucciones de nuestra sección crítica**

Sólo un proceso entrará a ejecutar las instrucciones de su sección crítica. El resto de procesos, encontrarán el **recurso bloqueado** y esperarán hasta que el que consiguién entrar en `finalice` y lo desbloquee. Para ello, se utiliará un objeto de tipo `FileLock`.

Se necesitará poder abrir el fichero con permisos de lectura y escritura. Utilizaremos un objeto de la clase `RandomAccessFile`. Asi no utilizaremos un objeto para leer (`FileReader`) y otro para escribir (`FileWriter`).

Por supuesto, se continuará gestionando las posibles excepciones, tal y como se ha aprendido en nociones previas de programación.

### Detalles

Se Abre el fichero en modo `rwd` (lectura-escritura y las escrituras seran llevadas directamente a disco).

Marcadas con comentarios, las instrucciones que forman la región critica son:

1. **Solicitamos el bloqueo** del canal de acceso al fichero.

2. **Leer** valor de fichero.

3. **Incrementar** valor.

4. **Escribir** valor.

5. **Liberar el bloqueo** del fichero.

La solicitud y liberación del bloqueo son las instrucciones que marcan el inicio y el fin de la región crítica. De hecho, son las instrucciones que crean y terminan la región critica.

**La región crítica creada, sólo protege al recurso**. Sólo protege los accesos al fichero y asegura que mientras que este proceso tiene el fichero bloqueado, ningún otro proceso accedera al fichero. Como el bloqueo no se libera entre la lectura y la
escritura, otro proceso no podrá leer el mismo valor que haya leído este proceso. **El resto de instrucciones que están entre el bloqueo y desbloqueo del canal no estan protegidas.**

Es por esto, que en el fichero con las salidas de los procesos (`log-escritores.txt`), pueden mezclarse los textos de salida de unos procesos con otros.

En próximos recursos de ejemplo (Hilos), se verá como crear secciones criticas que protegen todas las instrucciones que las contienen, ya que se crean a nivel de instrucciones y no a nivel de recurso.

Esto sera así, porque los hilos de un mismo proceso pueden compartir variables (y zonas de memoria). Las variables no las protege el sistema operativo como sucede con los ficheros. El lenguaje de programación nos proporcionara los mecanismos para crear regiones críticas a nivel de grupo de instrucciones; y todas ellas seran ejecutadas de forma atómica y exclusiva.

La protección a nivel de recurso proporcionada por el sistema operativo, siempre la tendremos disponible (si el lenguaje y el SO la tienen implementada); tanto para procesos, como para hilos de un proceso.

El método `lock()` de la clase `FileChannel`, nos devuelve un objeto de tipo FileLock que representa al bloqueo que ha obtenido el proceso sobre el canal de acceso al fichero. Para el sistema, el fichero aparecerá como abierto en uso exclusivo para ese proceso. Solo podremos solicitar el bloqueo de un fichero, si lo hemos abierto con permisos de escritura.

El metodo `lock()`, suspende al proceso hasta que esté disponible el bloqueo. Y el metodo `lock()` puede generar una excepción.

El metodo `FileChannel.tryLock()`, funciona igual que el método `lock()`, salvo porque, no bloquea al proceso. Tambien genera excepciones.

Por supuesto, siempre es mas recomendable sustituir métodos que puedan generar excepciones por sus equivalentes que no las generen. Pero debemos tener mucho cuidado con no crear en una espera activa que pueda bloquear el sistema.

```java
// Ejemplo de espera activa
while((bloqueo = raf.getChannel().tryLock() != null);
```

### Espera activa

**Espera activa**: se produce cuando un proceso, continuamente testea el estado de un recurso o variable. Lo que hace es consumir tiempo de CPU en una tarea poco productiva.

#### ¿Como evitar los problemas que puede provocar?

1. Sustituir la espera activa por métodos bloqueantes equivalentes. Sera el sistema el encargado de suspender el proceso hasta que el recurso esté disponible.

2. Limitar el número de intentos, que no sean infinitos. Por ejemplo, cuando intentamos acceder a una pagina web y el servidor esta caído, nuestro navegador, realiza varios intentos esperando un tiempo entre ellos; después de ese numero de intentos, nos notifica que no ha podido conectar y que podemos intentarlo después. Nosotros deberemos hacer lo mismo.


En nuestro caso, como disponemos del metodo `FileChannel.lock()` que suspende al proceso hasta que le sea concedido el bloqueo del fichero (caso A), no nos plantearnos utilizar el método `FileChannel.tryLock()`.

Utilizaremos `FileChannel.tryLock()` en los casos en los que queramos consultar si un fichero se puede o no bloquear (o si ya esta bloqueado), pero no para obtener el bloqueo del recurso.

### Prueba del código

Se trata de un proyecto maven. Asegúrate que tienes [maven](https://maven.apache.org/install.html) instalado y puedes ejecutarlo en consola (`mvn --version`). Sitúate en la raiz de este repo (en el mismo directorio que se encuentra el fichero `pom.xml`)

Ejecuta la siguiente sentencia en consola:

```bash
mvn clean package
```

Esto generará una carpeta en el directorio en el que te encuentras y ahí encontrarás la los ficheros jar: `lanzador` y `escritor`.

Se ha decidido que los procesos reciban los siguientes parámetros:

1. El número de orden de creación de ese proceso.
2. El nombre o ruta del archivo que se quiere que utilice.

Con estos dos parámetros, se busca poder ver de forma más clara qué sucede durante la ejecución de los procesos, y poder cambiar el fichero al que acceden los ficheros, para hacer distintas pruebas.

Se podría ejecutar `lanzador` de la siguiente manera:

```bash
java -jar lanzador nuevo.txt
```

Se revisará el contenido del fichero en el que han escrito los diferentes procesos.

En este versión (`conSincro`), en lugar de utilizar ficheros de texto y guardar el número como cadena ASCII; los procesos han guardado el valor en formato binario. Veremos el valor guardado en el fichero utilizando un editor hexadecimal.

Se puede hacer uso de la calculadora del sistema para pasar el valor de hexadecimal a decimal.

Si hemos creado 26 procesos y cada uno incrementa 100 veces el valor del fichero comenzando por 0, al final debe ser 2600 o 0A28 en hexadecimal.

Para ver el log de lo que han estado haciendo los procesos consulta el fichero `log-escritores.txt`.