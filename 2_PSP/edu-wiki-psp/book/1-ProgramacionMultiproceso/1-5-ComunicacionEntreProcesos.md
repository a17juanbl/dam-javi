# Comunicación entre Procesos

Como ya hemos comentado en más de una ocasión a lo largo de esta unidad, cada proceso tiene su espacio de direcciones privado, al que no pueden acceder el resto de procesos. Esto constituye un mecanismo de seguridad; imagina qué locura, si tienes un dato en tu programa y cualquier otro, puede modificarlo de cualquier manera. Tu programa generaría errores, como poco.

Por supuesto, nos damos cuenta de que, si cada proceso tiene sus datos y otros procesos no pueden acceder a ellos directamente, cuando otro proceso los necesite, tendrá que existir alguna forma de comunicación entre ellos.

![Imagen sobre el Aislamiento De La Region De Memoria De Cada Proceso](img/aislamientoDeLaRegionDeMemoriaDeCadaProceso.png)

> Comunicación entre procesos: un proceso da o deja información; recibe o recoge información.

Los lenguajes de programación y los sistemas operativos, nos proporcionan **primitivas** de sincronización que facilitan la interacción entre procesos de forma sencilla y eficiente.

Una primitiva, hace referencia a una operación de la cual conocemos sus restricciones y efectos, pero no su implementación exacta. Veremos que usar esas primitivas se traduce en utilizar objetos y sus métodos, teniendo muy en cuenta sus repercusiones reales en el comportamiento de nuestros procesos.

![Imagen sobre la comunicación entre procesos](img/comunicacionEntreProcesos.png)

**Clasificaremos las interacciones** entre los procesos y el resto del sistema (recursos y otros procesos), como estas tres:

* **Sincronización**: Un proceso puede conocer el punto de ejecución en el que se encuentra otro en ese determinado instante.

* **Exclusión mutua**: Mientras que un proceso accede a un recurso, ningún otro proceso accede al mismo recurso o variable compartida.

* **Sincronización condicional**: Sólo se accede a un recurso cuando se encuentra en un determinado estado interno.


## Mecanismos básicos de comunicación

Si pensamos en la forma en la que un proceso puede comunicarse con otro. Se nos ocurrirán estas dos:

* **Intercambio de mensajes**. Tendremos las primitivas enviar (send) y recibir (receive o wait) información.

* **Recursos (o memoria) compartidos**. Las primitivas serán escribir (write) y leer (read) datos en o de un recurso.

En el caso de comunicar procesos dentro de una misma máquina, el **intercambio de mensajes**, se puede realizar de dos formas:

* Utilizar un *buffer* de memoria.

* Utilizar un *socket*.

**La diferencia entre ambos**, está en que un *socket* se utiliza para intercambiar información entre procesos en distintas máquinas a través de la red; y un *buffer* de memoria, crea un canal de comunicación entre dos procesos utilizando la memoria principal del sistema. Actualmente, es más común el uso de sockets que buffers para comunicar procesos. Trataremos en profundidad los sockets en posteriores unidades. Pero veremos un par de ejemplos muy sencillos de ambos.

En java, utilizaremos **sockets y buffers** como si utilizáramos cualquier otro **stream** o flujo de datos. Utilizaremos los métodos read-write en lugar de send-receive.

**Con respecto a las lecturas y escrituras, debemos recordar, que serán bloqueantes**. Es decir, un proceso quedará bloqueado hasta que los datos estén listos para poder ser leídos. Una escritura, bloqueará al proceso que intenta escribir, hasta que el recurso no esté preparado para poder escribir. Aunque, esto está relacionado con el acceso a recursos compartidos, cosa que estudiaremos en profundidad, en el apartado *\"Regiones críticas\"*.


Pero, esto parece que va a ser algo complicado. ¿Cómo implementamos la comunicación entre procesos?

![Captura recurso Flash](doc/PSP01_CONT_R040_ComunicacionProcesos2/captura.png) [SWF](doc/PSP01_CONT_R040_ComunicacionProcesos2/flash.swf) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-comunicacionprocesos/)

### Para saber más sobre buffer de Memoria

Volvamos a nuestros buffers de memoria. **Un buffer de memoria, es creado por el SO en el instante en el que lo solicita un proceso**. El uso de buffers plantea un problema y es, que los buffers suelen crearse dentro del espacio de memoria de cada proceso, por lo que no son accesibles por el resto. Se puede decir, que no poseen una dirección o ruta que se pueda comunicar y sea accesible entre los distintos procesos, como sucede con un socket o con un fichero en disco.

**Una solución intermedia, soportada por la mayoría de los SO, es que permiten a los procesos utilizar archivos mapeados en memoria (memory-mapped file)**. Al utilizar un fichero mapeado en memoria, abrimos un fichero de disco, pero indicamos al SO que queremos acceder a la zona de memoria en la que el SO va cargando la información del archivo. El SO utiliza la zona de memoria asignada al archivo como buffer intermedio entre las operaciones de acceso que estén haciendo los distintos procesos que hayan solicitado el uso de ese archivo y el fichero físico en disco. Podemos ver los ficheros mapeados en memoria, como un fichero temporal que existe solamente en memoria (aunque sí tiene su correspondiente ruta de acceso a fichero físico en disco).

[Ampliar información sobre qué son, ventajas e inconvenientes del uso de ficheros mapeados en memoria.](https://es.wikipedia.org/wiki/Archivo_proyectado_en_memoria)

[En java, podemos utilizar ficheros mapeados en memoria utilizando la clase `java.nio.channels.FileChannel`](https://docs.oracle.com/javase/6/docs/api/java/nio/channels/FileChannel.html)

## Tipos de Comunicación

Ya hemos visto que dos procesos pueden comunicarse. Remarquemos algunos conceptos fundamentales sobre comunicación.

En cualquier comunicación, vamos a tener los siguientes elementos:

* *Mensaje*. Información que es el objeto de la comunicación.
* *Emisor*. Entidad que emite, genera o es origen del mensaje.
* *Receptor*. Entidad que recibe, recoge o es destinataria del mensaje.
* *Canal*. Medio por el que viaja o es enviado y recibido el mensaje.

Podemos **clasificar el canal de comunicación según su capacidad**, y los sentidos en los que puede viajar la información, como:

* *Símplex*. La comunicación se produce en un sólo sentido. El emisor es origen del mensaje y el receptor escucha el mensaje al final del canal. Ejemplo: reproducción de una película en una sala de cine.

* *Dúplex* (Full Duplex). Pueden viajar mensajes en ambos sentidos simultáneamente entre emisor y receptor. El emisor es también receptor y el receptor es también emisor. Ejemplo: telefonía.

* *Semidúplex (Half Duplex)*. El mensaje puede viajar en
ambos sentidos, pero no al mismo tiempo. Ejemplo: comunicación con walkie-talkies.

Otra **clasificación dependiendo de la sincronía que mantengan el emisor y el receptor** durante la comunicación, será:

* *Síncrona*. El emisor queda bloqueado hasta que el receptor recibe el mensaje. Ambos se sincronizan en el momento de la recepción del mensaje.

* *Asíncrona*. El emisor continúa con su ejecución inmediatamente después de emitir el mensaje, sin quedar bloqueado.

* *Invocación remota*. El proceso emisor queda suspendido hasta que recibe la confirmación de que el receptor recibido correctamente el mensaje, después emisor y receptor ejecutarán síncronamente un segmento de código común.

**Dependiendo del comportamiento que tengan los interlocutores que intervienen en la comunicación**, tendremos comunicación:

* *Simétrica*. Todos los procesos pueden enviar y recibir información.

* *Asimétrica*. Sólo un proceso actúa de emisor, el resto sólo escucharán el o los mensajes.

En nuestro anterior [ejemplo básico de comunicación con sockets](https://bitbucket.org/eduxunta/edu-java-psp-comunicacionprocesos/): el proceso `SocketEscritor`, era el emisor; el proceso `SocketLector`, era el receptor. El canal de comunicación: sockets. En el ejemplo, hemos utilizado del socket en una sola dirección y síncrona; pero los sockets permiten comunicación dúplex síncrona (en cada sentido de la comunicación) y simétrica (ambos procesos pueden escribir en y leer del socket); también existen otros tipos de sockets que nos permitirán establecer comunicaciones asimétricas asíncronas (`DatagramSocket`).

En el caso del [ejemplo de las tuberías](https://bitbucket.org/eduxunta/edu-java-psp-comunicacionprocesos/), la comunicación que se establece es simplex síncrona y asimétrica.
Nos damos cuenta, que **conocer las características de la comunicación que necesitamos establecer entre procesos, nos permitirá seleccionar el canal, herramientas y comportamiento más convenientes.**

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)
