# Programación en Java

La tecnología Java es:

* Lenguaje de programación Java es un lenguaje de **alto nivel**, **orientado a objetos**. El lenguaje es inusual porque los programas Java son tanto **compilados** como   **interpretados**. La compilación traduce el código Java a un lenguaje intermedio llamado Java bytecode. El Bytecode, es analizado y ejecutado (interpretado) por Java Virtual Machine (JVM)—un traductor entre **bytecode**, el sistema operativo subyacente y el hardware. Todas las implementaciones del lenguaje de programación deben emular JVM, para permitir que los programas Java se ejecuten en cualquier sistema que tenga una versión de JVM.

* La plataforma Java es una plataforma sólo de software que se ejecuta sobre varias plataformas de hardware. **Está compuesto por JVM y la interfaz de programación de aplicaciones (API) Java—un amplio conjunto de componentes de software (clases) listos para usar que facilitan el desarrollo y despliegue de applets y aplicaciones***. La API Java abarca desde objetos básicos a conexión en red, seguridad, generación de XML y servicios web. Está agrupada en bibliotecas—conocidas como paquetes—de clases e interfaces relacionadas.
Versiones de la plataforma:

	* Java SE (Plataforma Java, Standard Edition). Permite desarrollar y desplegar aplicaciones Java en desktops y servidores, como también en entornos empotrados y en tiempo real.

	* Java EE (Plataforma Java, Enterprise Edition). La versión empresarial ayuda a desarrollar y desplegar aplicaciones Java en el servidor portables, robustas, escalables y seguras.

	* Java ME (Plataforma Java, Micro Edition). Proporciona un entorno para aplicaciones que operan en una gama amplia de dispositivos móviles y empotrados, como teléfonos móviles, PDA, STB de TV e impresoras.

Para la implementación y desarrollo de aplicaciones, nos servimos de un IDE (**Entorno Integrado de Desarrollo**), que es un programa informático formado por distintas herramientas de programación; como son: editor, complilador, intérprete, depurador, control de versiones, ...

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)
