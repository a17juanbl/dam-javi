# Sincronización entre procesos


Ya tenemos mucho más claro, que las situaciones en las que dos o más procesos tengan que comunicarse, cooperar o utilizar un mismo recurso; implicará que deba haber cierto sincronismo entre ellos. O bien, unos tienen que esperar que otros finalicen alguna acción o tienen que realizar alguna tarea al mismo tiempo.

En este capítulo, veremos distintas problemáticas, **primitivas y soluciones de sincronización** necesarias para resolverlas. También es cierto, que en **el sincronismo entre procesos lo hace posible el SO y lo que hacen los lenguajes de programación de alto nivel es encapsular los mecanismos de sincronismo que proporciona cada SO en objetos, métodos y funciones**. Los lenguajes de programación, proporcionan primitivas de sincronismo entre los distintos hilos que tenga un proceso; estas primitivas del lenguaje, las veremos en la siguiente unidad.

Comencemos viendo un ejemplo muy sencillo de un problema que se nos plantea de forma más o menos común: inconsistencias en la actualización de un valor compartido por varios
procesos; así, nos daremos cuenta de la importancia del uso de mecanismos de sincronización.

**En programación concurrente, siempre que accedamos a algún recurso compartido (eso incluye a los ficheros), deberemos tener en cuenta las condiciones en las que nuestro proceso debe hacer uso de ese recurso: ¿será de forma exclusiva o no? Lo que ya definimos anteriormente como condiciones de competencia.**

En el caso de lecturas y escrituras en un fichero, debemos determinar si queremos acceder al fichero como sólo lectura; escritura; o lectura-escritura; y utilizar los objetos que nos permitan establecer los mecanismos de sincronización necesarios para que un proceso pueda bloquear el uso del fichero por otros procesos cuando él lo esté utilizando.

Esto se conoce como **el problema de los procesos lectores-escritores**. El sistema operativo, nos ayudará a resolver los problemas que se plantean; ya que:

* Si el acceso es de sólo lectura. Permitirá que todos los procesos lectores, que sólo quieren leer información del fichero, puedan **acceder simultáneamente** a él.

* En el caso de escritura, o lectura-escritura. El SO nos permitirá pedir un tipo de **acceso de forma exclusiva** al fichero. Esto significará que el proceso deberá esperar a que otros procesos lectores terminen sus accesos y otros procesos (lectores o escritores), esperarán a que ese proceso escritor haya finalizado su escritura.

Debemos tener en cuenta que, nosotros, nos comunicamos con el SO a través de los objetos y métodos proporcionados por un lenguaje de programación y, por lo tanto, tendremos que consultar cuidadosamente la documentación de las clases que estamos utilizando para conocer todas las peculiaridades de su comportamiento.

En la siguiente presentación, podemos ver cómo implementamos dos aplicaciones. Una de ellas, lee un valor de un fichero y lo escribe en el mismo fichero después de incrementarlo en uno. Otra aplicación crea un grupo de procesos de la primera aplicación; todos esos procesos accederán al mismo fichero para realizar la misma acción. Al final de la ejecución, al abrir el fichero que han estado utilizando, el valor que encontremos, ¿será el que esperamos que debe ser?

![Captura recurso Flash](doc/PSP01-CONT_R045_AccesosRecursoCompartidoSinSincro/captura.png) [SWF](doc/PSP01-CONT_R045_AccesosRecursoCompartidoSinSincro/flash.swf) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-accesorecursocompartido/src/master/)


## Regiones críticas

La definición común, y que habíamos visto anteriormente, de una región o **sección crítica, es, el conjunto de instrucciones en las que un proceso accede a un recurso compartido**. Para que la definición sea correcta, añadiremos que, **las instrucciones que forman esa región crítica, se ejecutarán de forma indivisible o atómica y de forma exclusiva con respecto a otros procesos que accedan al mismo recurso compartido al que se está accediendo**.

Al identificar y definir nuestras regiones críticas en el código, tendremos en cuenta:

* Se protegerán con secciones críticas **sólo aquellas instrucciones que acceden** a un recurso compartido.

* Las instrucciones que forman una sección crítica, **serán las mínimas**. Incluirán sólo las instrucciones imprescindibles que deban ser ejecutadas de forma atómica.

* Se pueden definir **tantas secciones críticas como sean necesarias**.

* Un único proceso entra en su sección crítica. **El resto de procesos esperan** a que éste salga de su sección crítica. El resto de procesos esperan, porque encontrarán el recurso bloqueado. El proceso que está en su sección crítica, es el que ha bloqueado el recurso.

* **Al final de cada sección crítica, el recurso debe ser liberado** para que puedan utilizarlo otros procesos.


Algunos lenguajes de programación permiten definir bloques de código como secciones críticas. Estos lenguajes, cuentan con palabras reservadas específicas para la definición de estas regiones. En Java, veremos cómo definir este tipo de regiones a nivel de hilo en posteriores unidades.

A nivel de procesos, haremos que nuestro ejemplo de accesos múltiples a un fichero sea correcto para su ejecución en un entorno concurrente. En esta presentación identificaremos la sección o secciones críticas y qué objetos debemos utilizar para conseguir que esas secciones se ejecuten de forma excluyente.

![Captura recurso Flash](doc/PSP01-CONT_R049-Proyectos_AccesosFicheroConSincro/captura.png)
[SWF](doc/PSP01-CONT_R049-Proyectos_AccesosFicheroConSincro/flash.swf) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-accesorecursocompartido/src/conSincro/) [WEB](doc/PSP01-CONT_R049-Proyectos_AccesosFicheroConSincro/flash.html)

En nuestro ejemplo hemos visto cómo definir una sección crítica para proteger las actualizaciones de un fichero. Cualquier actualización de datos en un recurso compartido necesitará establecer una región crítica, que implicará como mínimo estas instrucciones:

* **Leer el dato** que se quiere actualizar. Pasar el dato a la zona de memoria local al proceso. 
* **Realizar el cálculo** de actualización. Modificar el dato en memoria.
* **Escribir el dato actualizado**. Llevar el dato modificado de memoria al recurso compartido.

Debemos darnos cuenta de que nos referimos a un recurso compartido de forma genérica, ese recurso compartido podrá ser: memoria principal, fichero, base de datos, etc.

#### Para saber más

Además de bloquear un fichero completo, podemos solicitar al sistema que bloquee sólo una región del fichero, por lo que varios procesos pueden actualizar al mismo tiempo el fichero mientras que estén actualizando información que se encuentre en zonas distintas y no solapadas del fichero. [Ampliar información sobre los métodos `lock()` de la clase `FileChannel`](http://download.oracle.com/javase/6/docs/api/java/nio/channels/FileChannel.html%23lock()).

### Regiones críticas. Categoría de proceso cliente-suministrador

En este caso, vamos a hacer una introducción a los procesos que podremos clasificar dentro de la categoría cliente-suministrador.

* **Cliente**. Es un proceso que requiere o solicita información o servicios que proporciona otro proceso.

* **Suministrador**. Probablemente, te suene más el término servidor pero no queremos confundirnos con el concepto de servidor en el que profundizaremos en próximas unidades. Suministrador, hace referencia a un concepto de proceso más amplio; un suministrador, suministra información o servicios; ya sea a través memoria compartida, un fichero, red, o cualquier otro recurso.

* **Información o servicio perecederos**. La información desaparece cuando es consumida por el cliente; y, el servicio es prestado en el momento en el que cliente y suministrador están sincronizados.

![Ilustración cliente suministrador](img/cliente-suministrador.png)

**Entre un cliente y un suministrador (ojo, empecemos con un proceso de cada), se establece sincronismo entre ellos, por medio de intercambio de mensajes o a través de un recurso compartido**. Entre un cliente y un servidor, la comunicación se establece de acuerdo a un conjunto mensajes a intercambiar con sus correspondientes reglas de uso; llamado   protocolo. Podremos implementar nuestros propios protocolos o emplear protocolos existentes (ftp, http, telnet, smtp, pop3, ...); pero aún tenemos que ver algunos conceptos más antes de implementar protocolos.

Cliente y suministrador, son, los procesos que vimos en nuestros ejemplos de uso básico de sockets y comunicación a través de tuberías (apartado 4.1. Mecanismos básicos de comunicación) y, por supuesto, se puede extender a los casos en los que tengamos un proceso que lee y otro que escribe en un recurso compartido.

Entre procesos cliente y suministrador **debemos disponer de mecanismos de sincronización que permitan que**:

* Un *cliente* **no debe poder leer un dato hasta que no haya sido completamente suministrado**. Así nos aseguraremos de que el dato leído es correcto y consistente.

* Un *suministrador* irá produciendo su información, que en cada instante, **no podrá superar un volumen de tamaño máximo establecido**; por lo que el suministrador, no debe poder escribir un dato si se ha alcanzado ese máximo. Esto es así, para no desbordar al cliente.

Lo más sencillo, es pensar que el suministrador sólo produce un dato que el cliente tiene que consumir. ¿Qué sincronismo hace falta en esta situación?

1. El cliente tiene que esperar a que el suministrador haya generado el dato.

2. El suministrador genera el dato y de alguna forma avisa al cliente de que puede consumirlo.

![Ilustración cliente suministrador](img/cliente-suministrador-sincro.png)

Como podemos ver en este gráfico el pseudocódigo del cliente incluye el bucle que habíamos mencionado. Ese bucle hace que esta solución sea poco eficiente, ya que el proceso cliente estaría consumiendo tiempo de CPU sin realizar una tarea productiva; lo que conocemos como   **espera activa**. Además, si el proceso suministrador quedara bloqueado por alguna razón, ello también bloquearía al proceso cliente.

En los próximos apartados, vamos a centrarnos en los mecanismos de programación concurrente que nos permiten resolver estos problemas de sincronización entre procesos de forma eficiente, llamados **primitivas de programación concurrente**: *semáforos* y *monitores*; y son estas primitivas las que utilizaremos para proteger las secciones críticas de nuestros procesos.

## Semáforos

Veamos una primera solución eficiente a los problemas de sincronismo entre procesos que acceden a un mismo recurso compartido.

Podemos ver varios procesos que quieren acceder al mismo recurso, como coches que necesitan pasar por un cruce de calles. En nuestros cruces, los semáforos nos indican cuándo podemos pasar y cuándo no. Nosotros, antes de intentar entrar en el cruce, primero miramos el color en el que se encuentra el semáforo, y si está en verde (abierto), pasamos. Si el color es rojo (cerrado), quedamos a la espera de que ese mismo semáforo nos indique que podemos pasar. Este mismo funcionamiento es el que van a seguir nuestros semáforos en programación concurrente. Y, **son una solución eficiente, porque los procesos quedarán bloqueados (y no en espera activa) cuando no puedan acceder al recurso, y será el semáforo el que vaya desbloqueándolos cuando puedan pasar**.

> Un **semáforo**, es un componente de bajo nivel de abstracción que permite arbitrar los accesos a un recurso compartido en un entorno de programación concurrente.

Al utilizar un semáforo, lo veremos como un tipo dato, que podremos instanciar. Ese objeto semáforo podrá tomar un determinado conjunto de valores y se podrá realizar con él un conjunto determinado de operaciones. Un semáforo, tendrá también asociada una lista de procesos suspendidos que se encuentran a la espera de entrar en el mismo.

Dependiendo del conjunto de datos que pueda tomar un semáforo, tendremos:

* **Semáforos binarios**. Aquellos que pueden tomar sólo **valores 0 ó 1**. Como nuestras luces verde y roja.

* **Semáforos generales**. Pueden tomar **cualquier valor Natural** (entero no negativo). 

En cualquier caso, los valores que toma un semáforo representan:

* Valor **igual a 0**. Indica que el semáforo **está cerrado**. 
* Valor **mayor de 0**. El semáforo **está abierto**.

Cualquier semáforo permite dos operaciones seguras (la implementación del semáforo garantiza que la operación de chequeo del valor del semáforo, y posterior actualización según proceda, es siempre segura respecto a otros accesos concurrentes ):

* `objSemaforo.wait()`: Si el semáforo no es nulo (está abierto) decrementa en uno el valor del semáforo. Si el valor del semáforo es nulo (está cerrado), el proceso que lo ejecuta se suspende y se encola en la lista de procesos en espera del semáforo.

* `objSemaforo.signal()`: Si hay algún proceso en la lista de procesos del semáforo, activa uno de ellos para que ejecute la sentencia que sigue al wait que lo suspendió. Si no hay procesos en espera en la lista incrementa en 1 el valor del semáforo.

Además de la operación segura anterior, con un semáforo, también podremos realizar una operación no segura, que es la **inicialización del valor del semáforo**. Ese valor indicará **cuántos procesos pueden entrar concurrentemente en él**. Esta inicialización la realizaremos al crear el semáforo.

Para utilizar semáforos, **seguiremos los siguientes pasos**:

1. Un proceso padre creará e inicializará el semáforo.
2. El proceso padre creará el resto de procesos hijo pasándoles el semáforo que ha creado. Esos procesos hijos acceden al mismo recurso compartido.
3. Cada proceso hijo, hará uso de las operaciones seguras wait y signal respetando este esquema:
	
	1. `objSemaforo.wait()`; Para consultar si puede acceder a la sección crítica.
	2. Sección crítica; Instrucciones que acceden al recurso protegido por el semáforo objSemaforo.
	3. `objSemaforo.signal()`; Indicar que abandona su sección y otro proceso podrá entrar.
4. El proceso padre habrá creado tantos semáforos como tipos secciones críticas distintas se puedan distinguir en el funcionamiento de los procesos hijos (puede ocurrir, uno por cada recurso compartido).

La ventaja de utilizar semáforos es que **son fáciles de comprender**, proporcionan una **gran capacidad funcional** (podemos utilizarlos para resolver cualquier problema de concurrencia). Pero, su **nivel bajo de abstracción**, los hace **peligrosos de manejar** y, a menudo, son la **causa de muchos errores**, como es el interbloqueo. Un simple olvido o cambio de orden conduce a bloqueos; y requieren que la gestión de un semáforo se distribuya por todo el código lo que hace la depuración de los errores en su gestión sea muy difícil.

En java, encontramos la clase `Semaphore` dentro del paquete `java.util.concurrent`; y su uso real se aplica a los hilos de un mismo proceso, **para arbitrar el acceso de esos hilos** de forma concurrente a una misma región de la memoria del proceso. Por ello, veremos ejemplos de su uso en siguientes unidades de este módulo.

## Monitores

Los monitores, nos ayudan a resolver las desventajas que encontramos en el uso de semáforos. El problema en el uso de semáforos es que, recae sobre el programador o programadora la tarea de implementar el correcto uso de cada semáforo para la protección de cada recurso compartido y además ese recurso sigue estando disponible para utilizarlo sin la protección de un semáforo. Los monitores son como guardaespaldas, encargados de la protección de uno o varios recursos específicos pero a diferencia de los semáforos estos encierran esos recursos de forma que el proceso sólo puede acceder a dichos recursos a través de los métodos que el monitor expone.

>  Un monitor, es un componente de **alto nivel** de abstracción destinado a gestionar recursos que van a ser accedidos de forma concurrente.

**Los monitores encierran en su interior los recursos o variables compartidas como componentes privadas y garantizan el acceso a ellas en exclusión mutua**.

La declaración de un monitor incluye:

* Declaración de las constantes, variables, procedimientos y funciones que son **privados** del monitor (solo el monitor tiene visibilidad sobre ellos).

* Declaración de los **procedimientos y funciones** que el monitor expone (**públicos**) y **que constituyen la interfaz** a través de las que los procesos **acceden al monitor**.

* **Cuerpo del monitor**, constituido por un bloque de código que se ejecuta al ser instanciado o inicializado (el monitor). Su finalidad es inicializar las variables y estructuras internas del monitor.

* El monitor garantiza el acceso al código interno en **régimen de exclusión mutua**.

* Tiene **asociada una lista** en la que se incluyen los **procesos** que al tratar de acceder al monitor son **suspendidos**.

Los paquetes Java, no proporcionan una implementación de clase Monitor (habría que implementar un monitor para cada variable o recurso a sincronizar). Pero, siempre podemos implementarnos nuestra propia clase monitor, haciendo uso de semáforos para ello.

Pensemos un poco, ¿hemos utilizado objetos que pueden encajar con la declaración de un monitor aunque su tipo de dato no fuera monitor?, ¿no?, ¿seguro? Cuando realizamos una lectura o escritura en fichero, nuestro proceso queda bloqueado hasta que el sistema ha realizado completamente la operación. Nosotros inicializamos el uso del fichero indicando su ruta al crear el objeto, por ejemplo, `FileReader`; y utilizamos los métodos expuestos por ese objeto para realizar las operaciones que deseamos con ese fichero. Sin embargo, el código que realmente realiza esas operaciones es el implementado en la clase `FileReader`. Si bien, esos objetos no proporcionan exclusión mutua en los accesos al recurso o, por lo menos, no en todos sus métodos. Aún así, **podemos decir que utilicemos objetos de tipo monitor al acceder a los recursos del sistema, aunque no tengan como nombre Monitor**.

Las **ventajas** que proporciona el uso de monitores son:

* *Uniformidad*: El monitor provee una única capacidad, la exclusión mutua, no existe la confusión de los semáforos.

* *Modularidad*: El código que se ejecuta en exclusión mutua está separado, no mezclado con el resto del programa.

* *Simplicidad*: El programador o programadora no necesita preocuparse de las herramientas para la exclusión mutua .

* *Eficiencia de la implementación*: La implementación subyacente puede limitarse fácilmente a los semáforos.

Y, la **desventaja**:

* Interacción de múltiples condiciones de sincronización: Cuando el número de condiciones crece, y se
hacen complicadas, la complejidad del código crece de manera extraordinaria.

### Monitores: Lecturas y escrituras bloqueantes en recursos compartidos

Recordemos, el funcionamiento de los procesos cliente y suministrador podría ser el siguiente:

* Utilizan un recurso del sistema a modo de buffer compartido en el que, el suministrador introduce elementos y el cliente los extrae. 

* Se sincronizarán utilizando una variable compartida que indica el número de elementos que contiene ese buffer compartido, cuyo tamaño máximo será N.

* El proceso suministrador, siempre comprueba antes de introducir un elemento, que esa variable tenga un valor menor que N. Al introducir un elemento incrementa en uno la variable compartida.

* El proceso cliente, extraerá un elemento del buffer y decrementará el valor de la variable, siempre que el valor de la variable indique que hay elementos que consumir.


Los mecanismos de sincronismo que nos permiten el anterior funcionamiento entre procesos son; las **lecturas y escrituras bloqueantes en recursos compartidos del sistema (streams)**. En el caso de java, disponemos de:

* Arquitectura `java.io`.

	* **Implementación de clientes**: Para sus clases derivadas de Reader como son `InputStream`, `InputStreamReader`, `FileReader`, ... ; los métodos `read(buffer)` y `read(buffer, desplazamiento, tamaño)`.

	* **Implementación de suministradores**: Con sus análogos derivados de `Writer`; los métodos `write(info)` y `write(info, desplazamiento, tamaño)`.

* Arquitectura `java.nio` (disponible desde la versión 1.4 de Java). Dentro de `java.nio.channels`: 
	
	* **Implementación de clientes**: Sus clases `FileChannel` y `SocketChannel`; los métodos `read(buffer)` y `read(buffer, desplazamiento, tamaño)`.

	* **Implementación de suministradores**: Sus clases `FileChannel` y `SocketChannel`; los métodos `write(info)` y `write(info, desplazamiento, tamaño)`.


> Recordemos que, como vimos en el apartado 5.1 `Regiones críticas` tendremos que hacer uso del método `lock()` de `FileChannel` **para implementar las secciones críticas de forma correcta (tanto para suministradores como para clientes) cuando estemos utilizando un fichero como canal de comunicación entre ellos**.

![Captura recurso Flash](doc/PSP01-CONT_R056_ClienteSuministrador_fichero/captura.png)
[SWF](doc/PSP01-CONT_R056_ClienteSuministrador_fichero/flash.swf) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-clientesuministrador/src/master/) [WEB](doc/PSP01-CONT_R056_ClienteSuministrador_fichero/flash.html)

Si nos damos cuenta, hasta ahora sólo hemos hablado de un proceso Suministrador y un proceso cliente. El caso en el que **un suministrador tenga que dar servicio a más de un cliente**, aprenderemos a solucionarlo utilizando **hilos o threads**, e implementando esquemas de cliente-servidor, en las próximas unidades. No obstante, debemos tener claro, que el sincronismo cuando hay una información que genera un proceso (o hilo) y que recolecta otro proceso (o hilo) atenderá a las características que hemos descrito en estos aparatados.

### Para saber más

Dependiendo de la aplicación que vayamos a implementar, podemos encontrarnos en la necesidad de que el cliente pueda realizar otras operaciones mientras que no haya datos disponibles. Es decir, que las **operaciones de lectura no sean bloqueantes**. Un ejemplo claro, es la programación de un cliente de   streaming de música o vídeo. Ese cliente es un reproductor on-line que comienza a reproducir los datos que recibe, sin esperar a que se haya descargado completamente el archivo. Por supuesto, en estas situaciones tendremos que evaluar las variantes (con respecto a las que hemos visto en esta unidad) en el sincronismo de los procesos que se comunican.

Para conseguir lecturas no bloqueantes, haremos uso de la arquitectura java.nio, que implementa canales sobre sockets (SocketChannel). **Podremos configurar que las lecturas en un `SocketChannel` no sean bloqueantes por medio del método `configureBlocking(booleano)`, cuando booleano sea `false`.**

Las lecturas en un canal creado sobre un fichero (`FileSocket`) siempre son bloqueantes. [Ampliar información sobre el método configureBlocking(bool) de la clase SocketChannel](http://docs.oracle.com/javase/6/docs/api/java/nio/channels/spi/AbstractSelectableChannel.html%23configureBlocking(boolean)).

## Memoria Compartida

Una forma natural de comunicación entre procesos es la posibilidad de disponer de zonas de memoria compartidas (variables, buffers o estructuras). Además, los mecanismos de sincronización en programación concurrente que hemos visto: regiones críticas, semáforos y monitores; tienen su razón de ser en la existencia de recursos compartidos; incluida la memoria compartida.

Cuando se crea un proceso, el sistema operativo le asigna los recursos iniciales que necesita, siendo el principal recurso: la zona de memoria en la que se guardarán sus instrucciones, datos y pila de ejecución. Pero como ya hemos comentado anteriormente, **los sistemas operativos modernos, implementan mecanismos que permiten proteger la zona de memoria de cada proceso** siendo ésta **privada** para cada proceso, de forma que otros no podrán acceder a ella. Con esto, podemos pensar que no hay posibilidad de tener comunicación entre procesos por medio de memoria compartida. Pues, no es así. En la actualidad, la programación multihilo (que abordaremos en la siguiente unidad, se refiere, a tener varios flujos de ejecución dentro de un mismo proceso, compartiendo entre ellos la memoria asignada al proceso), nos permitirá examinar al máximo esta funcionalidad.

Pensemos ahora en un problemas que pueden resultar complicados si los resolvemos con un sólo procesador, por ejemplo: la ordenación de los elementos de una matriz. Ordenar una matriz pequeña, no supone mucho problema pero si la matriz se hace muy muy grande... Si disponemos de varios procesadores y somos capaces de partir la matriz en trozos (**convertir un problema grande en varios más pequeños**) de forma que cada procesador se encargue de ordenar cada parte de la matriz. Conseguiremos resolver el problema en menos tiempo, eso sí, teniendo en cuenta la complejidad de dividir el problema y asignar a cada procesador el conjunto de datos (o zona de memoria) que tiene que manejar y la tarea o proceso a realizar (y finalizar con la tarea de combinar todos los resultados para obtener la solución final). En este caso, tenemos sistemas multiprocesador como los actuales microprocesadores de varios núcleos, o los supercomputadores formados por múltiples ordenadores completos (e idénticos) trabajando como un único sistema. En ambos casos, contaremos con ayuda de sistemas específicos (sistemas operativos o entornos de programación), preparados para soportar la carga de computación en múltiples núcleos y/o equipos.

### Para saber más

Aunque en próximos apartados haremos una introducción a la programación paralela, es interesante conocer la existencia de **OpenMP** en este apartado. Es, una API para la programación multiproceso de memoria compartida en múltiples plataformas. Es un estándar disponible en muchas arquitecturas, incluidas las plataformas de Unix y de Microsoft Windows. Se compone de un conjunto de directivas de compilador, rutinas de biblioteca, y variables de entorno que influencian el comportamiento en tiempo de ejecución. OpenMP es un modelo de programación portable y escalable que proporciona a los programadores una interfaz simple y flexible para el desarrollo de aplicaciones paralelas para las plataformas que van desde las computadoras de escritorio hasta las supercomputadoras. También existe una implementación de OpenMP en Java. [Ampliar información sobre OpenMP](https://en.wikipedia.org/wiki/OpenMP).

## Cola de mensajes

El **paso de mensajes es una técnica** empleada en programación concurrente para aportar sincronización entre procesos y permitir la exclusión mutua, de manera similar a como se hace con los semáforos, monitores, etc. Su principal característica es **que no precisa de memoria compartida**.

Los elementos principales que intervienen en el paso de mensajes son el proceso que envía, el que recibe y el mensaje.

Dependiendo de si el proceso que envía el mensaje espera a que el mensaje sea recibido, se puede hablar de paso de mensajes síncrono o asíncrono:

* En el paso de **mensajes asíncrono**, el proceso que envía, no espera a que el mensaje sea recibido, y continúa su ejecución, siendo posible que vuelva a generar un nuevo mensaje y a enviarlo antes de que se haya recibido el anterior. Por este motivo se suelen emplear buzones o colas, en los que se almacenan los mensajes a espera de que un proceso los reciba. Generalmente empleando este sistema el proceso que envía mensajes sólo se bloquea o para cuando finaliza su ejecución o si el buzón está lleno. Para conseguir esto estableceremos una serie de reglas de comunicación (o protocolo) entre emisor y receptor, de forma que el receptor pueda indicar al emisor qué capacidad restante queda en su cola de mensajes y si está lleno o no.

* En el paso de **mensajes síncrono**, el proceso que envía el mensaje espera a que un proceso lo reciba para continuar su ejecución. Por esto se suele llamar a esta técnica encuentro o rendezvous. Dentro del paso de mensajes síncrono se engloba a la llamada a procedimiento remoto (RPC), muy popular en las arquitecturas cliente/servidor.

Veremos cómo implementar sincronización de procesos con paso de mensajes en las unidades 3, 4 y 5.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)