# Introducción

Una red de ordenadores o red informática la podemos definir como un sistema de comunicaciones que conecta ordenadores y otros equipos informáticos entre sí, con la finalidad de compartir información y   recursos.

Mediante la compartición de información y recursos en una red, los usuarios de la misma pueden hacer un mejor uso de ellos y, por tanto, mejorar el rendimiento global del sistema u organización.

Las principales ventajas de utilizar una red de ordenadores las podemos resumir en las siguientes:

* Reducción en el presupuesto para software y hardware. 
* Posibilidad de organizar grupos de trabajo.
* Mejoras en la administración de los equipos y las aplicaciones. 
* Mejoras en la integridad de los datos.
* Mayor seguridad para acceder a la información. 
* Mayor facilidad de comunicación.

Pues bien, **los servicios en red son responsables directos de estas ventajas**.

> **Un servicio en red es un software o programa que proporciona una determinada funcionalidad o utilidad al sistema**. Por lo general, estos programas están basados en un conjunto de   protocolos y estándares.

Una **clasificación de los servicios en red** atendiendo a su finalidad o propósito puede ser la siguiente:

* **Administración/Configuración**. Esta clase de servicios facilita la administración y gestión de las configuraciones de los distintos equipos de la red, por ejemplo: los servicios DHCP y DNS.

* **Acceso y control remoto**. Los servicios de acceso y control remoto, se encargan de permitir la conexión de usuarios a la red desde lugares remotos, verificando su identidad y controlando su acceso, por ejemplo Telnet y SSH.

* **De Ficheros**. Los servicios de ficheros consisten en ofrecer a la red grandes capacidades de almacenamiento para descongestionar o eliminar los discos de las   estaciones de trabajo, permitiendo tanto el almacenamiento como la transferencia de ficheros, por ejemplo FTP.

* **Impresión**. Permite compartir de forma remota impresoras de alta calidad, capacidad y coste entre múltiples usuarios, reduciendo así el gasto.

* **Información**. Los servicios de información pueden servir ficheros en función de sus contenidos, como pueden ser los documentos de   hipertexto, por ejemplo HTTP, o bien, pueden servir información para ser procesada por las aplicaciones, como es el caso de los servidores de bases de datos. 

* **Comunicación**. Permiten la comunicación entre los usuarios a través de mensajes escritos, por ejemplo email o correo electrónico mediante el protocolo SMTP.

![Ilustración de Servicios de Red](img/servicios.png)

A veces, **un servicio toma como nombre, el nombre del protocolo del nivel de aplicación en el que está basado**. Por ejemplo, hablamos de servicio FTP, por basarse en el protocolo FTP.

En esta unidad, verás ejemplos de cómo programar en Java algunos de estos servicios, pero antes vamos a ver qué son los protocolos del nivel de aplicación. Puedes ampliar información en este [recurso adicional](https://es.wikipedia.org/wiki/Servicio_de_red).

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)