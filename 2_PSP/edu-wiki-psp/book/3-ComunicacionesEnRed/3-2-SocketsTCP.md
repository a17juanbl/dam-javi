# Sockets TCP

**Los sockets permiten la comunicación entre procesos de diferentes equipos de una red. Un socket, es un punto de información por el cual un proceso puede recibir o enviar información**.

Tal y como hemos visto anteriormente, existen dos tipos de protocolos de comunicación: TCP o UDP. En el protocolo TCP es un protocolo orientado a la conexión que permite que un flujo de bytes originado en una máquina se entregue sin errores en cualquier máquina destino Por otro lado, el protocolo UDP, no orientado a conexión, se utiliza para comunicaciones donde se prioriza la velocidad sobre la pérdida de paquetes.

A la hora de crear un socket hay que tener claro el tipo de socket que se quiere crear (TCP o UDP). En esta sección vamos a aprender a utilizar los sockets TCP y en el siguiente punto veremos los sockets UDP.

Al utilizar sockets TCP, **el servidor utiliza un puerto por el que recibe las diferentes peticiones de los clientes**. Normalmente, el puerto del servidor es un puerto bajo `[1-1023]`.

![Ilustración Socket TCP-Conexion](img/socketTCPConexion.png)

**Cuando el cliente realiza la conexión con el servidor, a partir de ese momento se crea un nuevo socket que será en el encargado de permitir el envío y recepción de datos entre el cliente/servidor**. El puerto se crea de forma dinámica y se encuentra en el rango `49152-655335`. De ésta forma, el puerto por donde se reciben las conexiones de los clientes queda libre y cada comunicación tiene su propio socket.

![Ilustración transferencia de Datos TCP](img/transferenciaDatosTCP.png)

El paquete `java.net` de Java proporciona la clase `Socket` que permite la comunicación por red. De forma adicional, `java.net` incluye la clase `ServerSocket`, que **permite a un servidor escuchar y recibir** las peticiones de los clientes por la red. Y la clase `Socket` **permite a un cliente conectarse a un servidor para enviar y recibir** información.

## Servidor

Los pasos que realiza el servidor para realizar una comunicación son:

1. **Publicar puerto**. Se utiliza el comando ServerSocket indicando el puerto por donde se van a recibir las conexiones.

2. **Esperar peticiones**. En este momento el servidor queda a la espera a que se conecte un cliente. Una vez que se conecte un cliente se crea el socket del cliente por donde se envían y reciben los datos. 

3. **Envío y recepción de datos**. Para poder recibir/enviar datos es necesario crear un flujo (stream) de entrada y otro de salida. Cuando el servidor recibe una petición, éste la procesa y le envía el resultado al cliente.

4. Una vez finalizada la comunicación **se cierra el socket del cliente**.

![Ilustración sobre el funcionamiento interno del modelo cliente/servidor.png](img/funcionamientoInternoDelModeloClienteServidor.png)

Para publicar el puerto del servidor se utiliza la función `ServerSocket` a la que hay que indicarle el puerto a utilizar. Su estructura es:

`ServerSocket skServidor = new ServerSocket(Puerto);`

Una vez publicado el puerto, el servidor utiliza la **función `accept()`** para esperar la conexión de un cliente. **Una vez que el cliente se conecta, entonces se crea un nuevo socket por donde se van a realizar todas las comunicaciones con el cliente y servidor**.

`Socket sCliente = skServidor.accept();`

Una vez recibida la petición del cliente el servidor se comunica con el cliente a través de streams de datos que veremos en el siguiente punto.

Finalmente, una vez terminada la comunicación se cierra el socket de la siguiente forma:

`sCliente.close();`

A continuación se muestra el código comentado de un servidor:

```java
import java.io.* ; import java.net.* ; 

class Servidor {

	static final int Puerto=2000; public Servidor( ) {
		try {
			// Inicio la escucha del servidor en un determinado puerto
			ServerSocket skServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto );
			
			// Espero a que se conecte un cliente y creo un nuevo socket para el cliente 
			Socket sCliente = skServidor.accept();
			
			// ATENDER PETICIÓN DEL CLIENTE // Cierro el socket
			sCliente.close();
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
	}
	
	public static void main( String[] arg) {
	     new Servidor();
	} 
}
```

## Cliente

Los pasos que realiza el cliente para realizar una comunicación son:

1. **Conectarse con el servidor**. El cliente utiliza la función Socket para indicarse con un determinado servidor a un puerto específico. Una vez realizada la conexión se crea el socket por donde se realizará la comunicación.

2. **Envío y recepción de datos**. Para poder recibir/enviar datos es necesario crear un flujo (stream) de entrada y otro de salida.

3. Una vez finalizada la comunicación **se cierra el socket**.

![Ilustración sobre el funcionamiento interno del modelo cliente/servidor.png](img/funcionamientoInternoDelModeloClienteServidor.png)

Para conectarse a un servidor se utiliza la función Socket indicando el equipo y el puerto al que desea conectarse. Su sintaxis es:

`Socket sCliente = new Socket( Host , Puerto );`

donde `Host` es un string que guarda el nombre o dirección IP del servidor y `Puerto` es una variable del tipo int que guarda el puerto.

Si lo prefieres también puedes realizar la conexión directamente:

`Socket sCliente = new Socket("192.168.1.200", 1500);`

Una vez establecida la comunicación, se crean los streams de entrada y salida para realizar las diferentes comunicaciones entre el cliente y el servidor. En el siguiente apartado veremos la creación de streams.

Finalmente, una vez terminada la comunicación se cierra el socket de la siguiente forma:

`sCliente.close();`

A continuación se muestra el código comentado de un cliente:

```java
import java.io.*; import java.net.*; 

class Cliente {
	
	static final String Host = "localhost"; 
	static final int Puerto=2000;
	
	public Cliente() {
		try{
			// Me conecto al servidor en un detrminado puerto 
			Socket sCliente = new Socket( Host, Puerto );
			
			// TAREAS QUE REALIZA EL  CLIENTE
			
			// Cierro el socket
			sCliente.close();
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void main( String[] arg ) {
		new Cliente();
	}
}
```

## Flujo de Entrada y de Salida

Una vez establecida la conexión entre el cliente y el servidor se inicializa la variable del tipo `Socket` que en el ejemplo se llama `sCliente`. Para poder enviar o recibir datos a través del socket es necesario establecer un `stream` (flujo) de entrada o de salida según corresponda.

Continuando con el ejemplo anterior, a continuación se va a establecer un stream de salida llamado `flujo_salida`.

```java
OutputStream aux = sCliente.getOutputStream();
DataOutputStream flujo_salida= new DataOutputStream( aux );
```

o lo que es lo mismo,

```java
DataOutputStream flujo_salida= new
DataOutputStream(sCliente.getOutputStream());
```

A partir de éste momento puede enviar información de la siguiente forma:

`flujo_salida.writeUTF( "Enviar datos");`


De forma análoga, puede establecer el stream de entrada de la siguiente forma: 

```java
InputStream aux = sCliente.getInputStream();
DataInputStream flujo_entrada = new DataInputStream( aux );
```

o lo que es lo mismo,

```java
DataInputStream flujo_entrada = new
DataInputStream(sCliente.getInputStream());
```

A continuación se muestra una forma cómoda de recibir información:

```java
String datos=new String();
Datos=flujo_entrada.readUTF();
```

## [Ejemplo](https://bitbucket.org/eduxunta/edu-java-psp-sockets/src/master/samplesTCP/sample01-socketTCP/)

Para continuar con el ejemplo anterior y poder utilizar los sockets para enviar información vamos a realizar un ejemplo muy sencillo en el que el servidor va a aceptar tres clientes (de forma secuencial no concurrente) y le va a indicar el número de cliente que es.

### **`Servidor.java`**

```java
import java.io.*;
import java.net.*;

class Servidor {
	static final int Puerto = 2000;

	public Servidor() {
		try {
			@SuppressWarnings("resource")
			ServerSocket skServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto);
			for (int nCli = 0; nCli < 3; nCli++) {
				Socket sCliente = skServidor.accept();
				System.out.println("Sirvo al cliente " + nCli);
				OutputStream aux = sCliente.getOutputStream();
				DataOutputStream flujo_salida = new DataOutputStream(aux);
				flujo_salida.writeUTF("Hola cliente " + nCli);
				sCliente.close();
			}
			System.out.println("Ya se han atendido los 3 clientes");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] arg) {
		new Servidor();
	}
}
```

La salida que provocará el servidor en `System.out` será:

```bash
# En el momento en el que se inicie el server
Escucho el puerto 2000

# En el momento en que el primer cliente realice una petición
Sirvo al cliente 0

# En el momento en que el segundo cliente realice una petición
Sirvo al cliente 1

# En el momento en que el tercer cliente realice una petición
Sirvo al cliente 2

# En el momento en que un cuarto cliente realice una petición
Ya se han atendido los 3 clientes
```

### **`Cliente.java`**

```java
import java.io.*;
import java.net.*;

class Cliente {
	static final String HOST = "localhost";
	static final int Puerto = 2000;

	public Cliente() {
		try {
			Socket sCliente = new Socket(HOST, Puerto);
			InputStream aux = sCliente.getInputStream();
			DataInputStream flujo_entrada = new DataInputStream(aux);
			System.out.println(flujo_entrada.readUTF());
			sCliente.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] arg) {
		new Cliente();
	}
}
```

La salida que provocará los clientes en `System.out` será:

```bash
# Instanciando Cliente por primera vez
Hola cliente 0

# Instanciando Cliente por segunda vez
Hola cliente 1

# Instanciando Cliente por tercera vez
Hola cliente 2

# Instanciando Cliente por cuarta vez
Connection refused
```

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)