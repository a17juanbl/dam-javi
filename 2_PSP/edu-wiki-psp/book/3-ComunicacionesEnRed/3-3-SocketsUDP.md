# Sockets UDP

En el caso de utilizar sockets UDP no se crea una conexión (como es el caso de socket TCP) y básicamente permite enviar y recibir mensajes a través de una dirección IP y un puerto. Estos mensajes se gestionan de forma individual y no se garantiza la recepción o envío del mensaje como sí ocurre en TCP.

Para utilizar sockets UDP en java tenemos la clase [`DatagramSocket`](https://docs.oracle.com/javase/7/docs/api/java/net/DatagramSocket.html) y para recibir o enviar los mensajes se utiliza clase [`DatagramPacket`](https://docs.oracle.com/javase/7/docs/api/java/net/DatagramPacket.html). Cuando se recibe o envía un paquete se hace con la siguiente información: mensaje, longitud del mensaje, equipo y puerto.

## Receptor

En el caso de querer iniciar el socket en un determinado puerto se realiza de la siguiente forma:

`DatagramSocket sSocket = new DatagramSocket(puerto);`

Una vez iniciado el socket ya estamos en disposición de recibir mensajes utilizando la clase `DatagramPacket`. Cuando se recibe o envía un paquete se hace con la siguiente información: mensaje, longitud del mensaje, equipo y puerto.

A continuación se muestra un código de ejemplo para recibir un mensaje:

```java
byte [] cadena = new byte[1000] ;
DatagramPacket mensaje = new DatagramPacket(cadena, cadena.length); sSocket.receive(mensaje);
```

Una vez recibido el mensaje puede mostrar su contenido de la siguiente forma: 

```java
String datos=new String(mensaje.getData(),0,mensaje.getLength()); System.out.println("\tMensaje Recibido: " +datos);
```

Finalmente, una vez terminado el programa cerramos el socket:

`sSocket.close();`

## Emisor

Por otro lado, para realizar una aplicación emisora de mensajes UDP debe inicializar primero la estructura `DatagramSocket`.

`DatagramSocket sSocket = new DatagramSocket();`

Ahora debe crear el mensaje del tipo `DatagramPacket` al que debe indicar:

1. Mensaje a enviar.
2. Longitud del mensaje.
3. Equipo al que se le envía el mensaje. 
4. Puerto destino.

A continuación se muestra un ejemplo para crear un mensaje:

DatagramPacket mensaje = new DatagramPacket(mensaje,longitud_mensaje, Equipo, Puerto);

Para obtener la dirección del equipo al que se le envía el mensaje a través de su nombre se utiliza la función getByName de la clase InetAddress de la siguiente forma:

`InetAddress Equipo = InetAddress.getByName("localhost");`

Una vez creado el mensaje lo enviamos con la función `send()`: 

`sSocket.send(mensaje);`

Finalmente, una vez terminado el programa cerramos el socket:

`sSocket.close();`

## [Ejemplo](https://bitbucket.org/eduxunta/edu-java-psp-sockets/src/master/samplesUDP/sample01-socketUDP/)

A continuación, para aprender a programar Sockets UDP se va a realizar un ejemplo sencillo donde intervienen dos procesos:

* **ReceptorUDP**. Inicia el puerto `1500` y muestra en pantalla todos los mensajes que llegan a él. 
* **EmisorUDP**. Permite enviar por líneas de comandos mensajes al receptor por el puerto `1500`.

### `ReceptorUDP.java`

```java
import java.net.*;
import java.io.*;

public class ReceptorUDP {
  public static void main(String args[]) {
    // Sin argumentos
    if (args.length != 0) {
      System.err.println("Uso: java ReceptorUDP");
    } else
      try {
        // Crea el socket
        DatagramSocket sSocket = new DatagramSocket(1500);
        // Crea el espacio para los mensajes
        byte[] cadena = new byte[1000];
        DatagramPacket mensaje = new DatagramPacket(cadena, cadena.length);
        System.out.println("Esperando mensajes..");
        while (true) {
          // Recibe y muestra el mensaje
          sSocket.receive(mensaje);
          String datos = new String(mensaje.getData(), 0, mensaje.getLength());
          System.out.println("\tMensaje Recibido: " + datos);
        }
      } catch (SocketException e) {
        System.err.println("Socket: " + e.getMessage());
      } catch (IOException e) {
        System.err.println("E/S: " + e.getMessage());
      }
  }
}
```

El ReceptorUDP volcará en `System.out`:

```bash
# Al arrancar
Esperando mensajes..

# Al recibir un mensaje por parte de EmisorUDP
        Mensaje Recibido: hola
```

### `EmisorUDP.java`

```java
import java.net.*;
import java.io.*;

public class EmisorUDP {
    public static void main(String args[]) {
        // Comprueba los argumentos
        if (args.length != 2) {
            System.err.println("Uso: java EmisorUDP maquina mensaje");
        } else
            try {
                // Crea el socket
                DatagramSocket sSocket = new DatagramSocket();
                // Construye la dirección del socket del receptor
                InetAddress maquina = InetAddress.getByName(args[0]);
                int puerto = 1500;
                // Crea el mensaje
                byte[] cadena = args[1].getBytes();
                DatagramPacket mensaje = new DatagramPacket(cadena, args[1].length(), maquina, puerto);
                // Envía el mensaje
                sSocket.send(mensaje);
                // Cierra el socket
                sSocket.close();
            } catch (UnknownHostException e) {
                System.err.println("Desconocido: " + e.getMessage());
            } catch (SocketException e) {
                System.err.println("Socket: " + e.getMessage());
            } catch (IOException e) {
                System.err.println("E/S: " + e.getMessage());
            }
    }
}
```

Uso:

`java EmisorUDP <equipo> <mensaje>`

El `EmisorUDP` procesa los argumentos:

* `arg[0]`: como la máquina receptora del mensaje. P. ej: `localhost`
* `arg[1]`: como el mensaje a enviar. P. ej: `hola`

No escribirá nada en `System.out`.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)