# Aplicaciones Multihilo

Una aplicación multihilo debe reunir las siguientes propiedades:

* **Seguridad**. La aplicación no llegará a un estado inconsistente por un mal uso de los recursos compartidos. Esto implicará sincronizar hilos asegurando la exclusión mutua.
* **Viveza**. La aplicación no se bloqueará o provocará que un hilo no se pueda ejecutar. Esto implicará un comportamiento no egoísta de los hilos y ausencia de interbloqueos e inanición.

**La corrección de la aplicación** se mide en función de las propiedades anteriores, pudiendo tener:

* **Corrección parcial**. Se cumple la propiedad de seguridad. El programa termina y el resultado es el deseado.

* **Corrección total**. Se cumplen las propiedades de seguridad y viveza. El programa termina y el resultado es el correcto.

Por tanto, al desarrollar una aplicación multihilo **habrá que tener en cuenta los siguientes aspectos**:

1. La situación de los hilos en la aplicación: hilos independientes o colaborando/compitiendo. 
	* *Independientes*. No será necesario sincronizar y/o comunicar los hilos.
	* *Colaborando y/o compitiendo*. Será necesario sincronizar y/o comunicar los hilos, evitando interbloqueos y esperas indefinidas.

2. Gestionar las prioridades, de manera que los hilos más importantes se ejecuten antes. 
3. No todos los Sistemas Operativos implementan *time-slicing*.
4. La ejecución de hilos es no-determinística.

Por lo general, **las aplicaciones multihilo son más difíciles de desarrollar y complicadas de depurar** que una aplicación secuencial o de un solo hilo; pero si utilizamos las librerías que aporta el lenguaje de programación, podemos obtener algunas ventajas:

* **Facilitar la programación**. Requiere menos esfuerzo usar una clase estándar que desarrollarla para realizar la misma tarea.

* **Mayor rendimiento**. Los algoritmos utilizados han sido desarrollados por expertos en concurrencia y rendimiento.

* **Mayor fiabilidad**. Usar librerías o bibliotecas estándar, que han sido diseñadas para evitar interbloqueos (deadlocks), cambios de contexto innecesarios o condiciones de carrera, nos permiten garantizar un mínimo de calidad en nuestro software.

* **Menor mantenimiento**. El código que generemos será más legible y fácil de actualizar.

* **Mayor productividad**. El uso de una API estándar permite mejor coordinación entre desarrolladores y reduce el tiempo de aprendizaje.

Teniendo en cuenta esto último, cuando vayas a desarrollar una aplicación multihilo debes hacer uso de las utilidades que ofrece el propio lenguaje. Esto facilitará la puesta a punto del programa y su depuración.

## Otras utilidades de concurrencia

Además de las utilidades de sincronización que hemos visto en apartados anteriores, el paquete `java.util.concurrent` incluye estas otras utilidades de concurrencia:

* La interfaz Executor 
* Colecciones.
* La clase Locks 
* Variables atómicas

**El programador de tareas `Executor`** es una interfaz que permite:

* Realizar la ejecución de tareas en un único hilo en segundo plano
(como eventos *Swing*), en un hilo nuevo, o en un pool de hilos
* Diseñar políticas propias de ejecución y añadirlas a `Executor`.
* Ejecutar tareas mediante el método `execute()`. Estas tareas tienen que implementar la interfaz `Runnable`.
* Hacer uso de diferentes implementaciones de `Executor`, como `ExecutorService`. 

Entre las **colecciones** hay que destacar:

* La interfaz `Queque`. Es una colección diseñada para almacenar elementos antes de procesarlos, ofreciendo diferentes operaciones como inserción, extracción e inspección.
 
* La interfaz `BlockingQueque`, diseñada para colas de tipo productor/consumidor, y que son *thread-safe* (aseguran un funcionamiento correcto de los accesos simultáneos multihilo a recursos compartidos). Son capaces de esperar mientras no haya elementos almacenados en la cola.

* Implementaciones concurrentes de `Map` y `List`.

**La clase de bloqueos**, `java.util.concurrent.locks`, proporciona diferentes implementaciones y diversos tipos de bloqueos y desbloqueos entre métodos. Su funcionalidad es equivalente a `Synchronized`, pero proporciona métodos que hacen más fácil el uso de bloqueos y condiciones. Entre ellos.

* El métdo `newCondition()`, que permite tener un mayor control sobre el bloqueo y genera un objeto del tipo `Condition` asociado al bloqueo. Así el método `await()` indica cuándo deseamos esperar, y el método `signal()` permite indicar si una condición del bloqueo se activa, para finalizar la espera.

* La implementación `ReentranLock`, permite realizar exclusión mutua utilizando monitores. El método `lock()` indica que deseamos utilizar el recurso compartido, y el método `unlock()` indica que hemos terminado de utilizarlo.

**Las variables atómicas** incluidas en las utilidades de concurrencia clase `java.util.concurrent.atomic`, permiten definir recursos compartidos, sin la necesidad de proteger dichos recursos de forma explícita, ya que ellas internamente realizan dichas labores de protección.

Si desarrollamos aplicaciones multihilo más complejas, por ejemplo para plataformas *multiprocesador* y sistemas con *multi-núcleo* (*multi-core*) que requieren un alto nivel de concurrencia, será muy conveniente hacer uso de todas estas utilidades.

## La interfaz `Executor`y los pools de hilos

Cuando trabajamos con **aplicaciones tipo servidor**, éstas tienen que atender un número masivo y concurrente de peticiones de usuario, en forma de tareas que deben ser procesadas lo antes posible. Al principio de la unidad ya te indicamos mediante un ejemplo la conveniencia de utilizar hilos en estas aplicaciones, pero si ejecutamos cada tarea en un hilo distinto, se pueden llegar a crear tantos hilos que el incremento de recursos utilizados puede comprometer la estabilidad del sistema. Los pools de hilos ofrecen una solución a este problema.

Y ¿qué es un pool de hilos? **Un pool de hilos** (*thread pools*) es básicamente un contenedor dentro del cual se crean y se inician un número limitado de hilos, para ejecutar todas las tareas de una lista.

Para **declarar un pool**, lo más habitual es hacerlo **como un objeto del tipo `ExecutorService`** utilizando alguno de los siguientes métodos de la clase estática `Executors`:

* `newFixedThreadPool(int numeroHilos)`: crea un pool con el número de hilos indicado. Dichos hilos son reutilizados cíclicamente hasta terminar con las tareas de la cola o lista.
* `newCachedThreadPool()`: crea un pool que va creando hilos conforme se van necesitando, pero que puede reutilizar los ya concluidos para no tener que crear demasiados. Los hilos que llevan mucho tiempo inactivos son terminados automáticamente por el pool.
* `newSingleThreadExecutor()`: crea un pool de un solo hilo. La ventaja que ofrece este esquema es que si ocurre una excepción durante la ejecución de una tarea, no se detiene la ejecución de las siguientes. 
* `newScheduledExecutor()` : crea un pool que va a ejecutar tareas programadas cada cierto tiempo, ya sea una sola vez o de manera repetitiva. Es parecido a un objeto Timer, pero con la diferencia de que puede tener varios threads que irán realizando las tareas programadas conforme se desocupen.

Los objetos de tipo `ExecutorService` implementan la interfaz `Executor`. Esta interfaz define el método `execute(Runnable)`, al que hay que llamar una vez por cada tarea que deba ser ejecutada por el pool (la tarea se pasa como argumento del método).

La interface `ExecutorService` proporciona una serie de métodos para el control de la ejecución de las tareas, entre ellos el método `shutdown()`, para indicarle al pool que los hilos no se van a reutilizar para nuevas tareas y deben morir cuando finalicen su trabajo.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-executor/) dispones de un ejemplo en el que se define una clase que implementa `Runnable` cuya tarea es generar e imprimir 10 números aleatorios. Se creará un pool de 2 hilos capaz de realizar 30 de esas tareas.

## Gestión de excepciones

¿Cómo podemos gestionar la excepciones de una aplicación multihilo?

Para gestionar las excepciones de una aplicación multihilo puedes utilizar el método `uncaughtExceptionHandler()` de la clase `Thread`, que permite definir un manejador de excepciones.

Para crear un **manejador de excepciones** haremos lo siguiente:

1. Crear una clase que implemente la interfaz `Thread.UncaughtExceptionHandler`.
2. Implementar el método `uncaughtException()`.

Por ejemplo, podemos crear un manejador de excepciones que utilizarán todos los hilos de una misma aplicación de la siguiente forma:

El manejador sólo mostrará qué hilo ha producido la excepción y la pila de llamadas de la excepción.

```java
public class ManejadorExcepciones implements Thread.
//manejador de excepciones para toda la aplicación
   UncaughtExceptionHandler{
    //implementa el método uncaughtException()
    public void uncaughtException(Thread t, Throwable e){
        System.out.printf("Thread que lanzó la excepción: %s \n", t.getName());
        //muestra en consola el hilo que produce la exceción
        e.printStackTrace();
        //muestra en consola la pila de llamadas
    }
}
```

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-ManejoExceptions/) te facilitamos el proyecto completo. Se creará el anterior manejador de excepciones y se implementará un hilo que divide el número 100 por un número aleatorio comprendido entre 0 y 4, dando así la posibilidad de dividir por 0. Se crean e inician 5 hilos que harán uso del manejador.

## Depuración y documentación

Dos tareas muy importantes cuando desarrollamos software de calidad son la depuración y documentación de nuestras aplicaciones.

* Mediante la **depuración** trataremos de corregir fallos y errores de funcionamiento del programa.
* Mediante la **documentación interna** aportaremos legibilidad a nuestros programas.

**La depuración de aplicaciones multihilo** es una tarea difícil debido a que:

* La ejecución de los hilos tiene un comportamiento no determinístico.
* Hay que controlar varios flujos de ejecución.
* Aparecen nuevos errores potenciales debidos a la compartición de recursos entre varios hilos: 
	* Errores porque no se cumple la exclusión mutua.
	* Errores porque se produce interbloqueo.

Podemos realizar seguimientos de la pila de Java tanto estáticos como dinámicos, utilizando los siguientes métodos de la clase thread:

* `dumpStack()`. Muestra una traza de la pila del hilo (thread) en curso.

* `getAllStackTraces()`. Devuelve un Map de todos los hilos vivos en la aplicación. (`Map` es la interfaz hacia objetos `StackTraceElement`, que contiene el nombre del fichero, el número de línea, y el nombre de la clase y el método de la línea de código que se está ejecutando).

* `getStackTrace()`. Devuelve el seguimiento de la pila de un hilo en una aplicación.

Tanto `getAllStackTraces()` como `getStackTrace()` permiten grabar los datos del seguimiento de pila en un log.

En el siguiente [recurso adicional sobre cómo depurar aplicaciones multihilo con Netbeans](https://netbeans.org/kb/docs/java/debug-multithreaded.html) encontrarás un ejemplo de cómo depurar aplicaciones multihilo desde este IDE. Te puedes descargar además las aplicaciones de ejemplo para practicar.

A la hora de **documentar una aplicación multihilo** no debemos escatimar en comentarios. Si son importantes en cualquier aplicación, con más motivo en una aplicación multihilo, debido a su mayor complejidad.

Para documentar nuestra aplicación Java, utilizaremos el generador `JavaDoc`, que de forma automática genera la documentación de la aplicación a partir del código fuente. Como ya debes de saber si has estudiado el *módulo de Programación*, este sistema consiste en incluir comentarios en el código, utilizando las etiquetas `/**` y `*/`, que después pueden procesarse y generar un conjunto de páginas navegables HTML.

Te recordamos mediante el [recurso adicional en forma de screencast](doc/generating_javadoc_in_netbeans_ide.mp4) (debes tener instalado JavaDoc en tu equipo, cómo obtener la documentación de tu programa con JavaDoc y Netbeans a partir de tu código fuente.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)