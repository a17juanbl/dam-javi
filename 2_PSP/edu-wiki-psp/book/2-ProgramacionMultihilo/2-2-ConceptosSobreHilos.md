# Conceptos sobre hilos

Pero ¿qué es realmente un hilo? **Un hilo, denominado también subproceso, es un flujo de control secuencial independiente dentro de un proceso y está asociado con una *secuencia de instrucciones*, un conjunto de *registros* y una *pila***.

Cuando se ejecuta un programa, el Sistema Operativo crea un proceso y también crea su primer hilo, el hilo primario, el cual puede a su vez crear hilos adicionales.

Desde este punto de vista, **un proceso no se ejecuta, sino que solo es el *espacio de direcciones* donde reside el código que es ejecutado mediante uno o más hilos**.

Por lo tanto podemos hacer las siguientes **observaciones**:

* Un hilo no puede existir independientemente de un proceso.
* Un hilo no puede ejecutarse por si solo.
* Dentro de cada proceso puede haber varios hilos ejecutándose.

![Ilustración procesos e hilos](img/procesosAndHilos.png)

Un único hilo es similar a un programa secuencial, por si mismo no nos ofrece nada nuevo. Es la habilidad de ejecutar varios hilos dentro de un proceso lo que ofrece algo nuevo y útil, ya que cada uno de estos hilos puede ejecutar actividades diferentes al mismo tiempo. Así en un programa un hilo puede encargarse de la comunicación con el usuario, mientras que otro hilo transmite un fichero, otro puede acceder a recursos del sistema (cargar sonidos, leer ficheros, ...), etc.

![Ilustración procesos con hilos](img/programaVariosHilos.png)

Extiende la documentación con la [definición de Hilo de ejecución](https://es.wikipedia.org/wiki/Hilo_(informática)) en la Wikipedia.

## Recursos compartidos por los hilos

![Ilustración memoria en procesos Multihilo](img/memoriaProcesosMultiHilo-1.png)

![Ilustración memoria en procesos Multihilo](img/memoriaProcesosMultiHilo-2.png)

## Ventajas y uso de hilos

Como consecuencia de compartir el espacio de memoria, **los hilos aportan las siguientes ventajas sobre los procesos**:

* Se consumen menos recursos en el lanzamiento, y la ejecución de un hilo que en el lanzamiento y ejecución de un proceso.

* Se tarda menos tiempo en crear y terminar un hilo que un proceso.

* La conmutación entre hilos del mismo proceso o cambio de contexto es bastante más rápida que entre procesos.

Es por esas razones, por lo que a los hilos se les denomina también **procesos ligeros**.

Y ¿cuándo se **aconseja utilizar hilos**? Se aconseja utilizar hilos en una aplicación cuando:

* La aplicación maneja **entradas de varios dispositivos** de comunicación.

* La aplicación debe poder realizar diferentes **tareas a la vez**.

* Interesa diferenciar tareas con una **prioridad** variada. Por ejemplo, una prioridad alta para manejar tareas de tiempo crítico y una prioridad baja para otras tareas.

* La aplicación se va a ejecutar en un **entorno multiprocesador**.

![Ilustración proceso vs hilo](img/procesoVShilo.png)

* Por ejemplo, imagina la siguiente situación:


	* Debes crear una aplicación 	que se ejecutará en un servidor para atender peticiones de clientes. Esta aplicación podría ser un servidor de bases de datos, o un servidor web.

	* Cuando se ejecuta el programa éste abre su puerto y queda a la escucha, esperando recibir peticiones. 

	* Si cuando recibe una petición de un cliente se pone a procesarla para obtener una respuesta y devolverla, cualquier petición que reciba mientras tanto no podrá atenderla, puesto que está ocupado.
	
	* La solución será construir la aplicación con múltiples hilos de ejecución.
	
	* En este caso, al ejecutar la aplicación se pone en marcha el hilo principal, que queda a la escucha. 
	
	* Cuando el hilo principal recibe una petición, creará un nuevo hilo que se encarga de procesarla y generar la consulta, mientras tanto el hilo principal sigue a la escucha recibiendo peticiones y creando hilos.
	
	* De esta manera un gestor de bases de datos puede atender consultas de varios clientes, o un servidor web puede atender a miles de clientes.
	
	* Si el número de peticiones simultáneas es elevado, la creación de un hilo para cada una de ellas puede comprometer los recursos del sistema. En este caso, como veremos al final de la unidad lo resolveremos mejor con un pool de hilos.

Resumiendo, los hilos son idóneos para programar aplicaciones de entornos interactivos y en red, así como simuladores y animaciones.

> Los hilos son más frecuentes de lo que parece. De hecho, todos los programas con interfaz gráfico son multihilo porque los eventos y las rutinas de dibujado de las ventanas corren en un hilo distinto al principal. Por ejemplo en Java, `AWT` o la biblioteca gráfica `Swing` usan hilos.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)

