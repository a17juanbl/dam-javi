# Estados de un hilo

El ciclo de vida de un hilo comprende los diferentes estados en los que puede estar un hilo desde que se crea o nace hasta que finaliza o muere.

De manera general, **los diferentes estados en los que se puede encontrar un hilo son los siguientes**:

* **Nuevo (new)**: se ha creado un nuevo hilo, pero aún no está
disponible para su ejecución.

* **Ejecutable (runnable)**: el hilo está preparado para ejecutarse.
Puede estar Ejecutándose, siempre y cuando se le haya asignado tiempo de procesamiento, o bien que no esté ejecutándose en un instante determinado en beneficio de otro hilo, en cuyo caso estará Preparado.

* **No Ejecutable o Detenido (no runnable)**: el hilo podría estar ejecutándose, pero hay alguna actividad interna al propio hilo que se lo impide, como por ejemplo una espera producida por una operación de Entrada/Salida (E/S). Si un hilo está en estado "No Ejecutable", no tiene oportunidad de que se le
asigne tiempo de procesamiento.

* **Muerto o Finalizado (terminated)**: el hilo ha finalizado. La forma natural de que muera un hilo es finalizando su método `run()`.

El método `getState()` de la clase `Thread`, permite obtener en cualquier momento el estado en el que se encuentra un hilo. Devuelve por tanto: `NEW`, `RUNNABLE`, `NO RUNNABLE` o `TERMINATED`.

![Ilustración Estados de un hilo](img/estadosDeUnHilo.png)

En la imagen anterior, puedes ver algunos de los métodos que permiten obtener cada uno de esos estados. Los veremos con más detalle en los siguientes apartados.

## Iniciar un hilo

Cuando se crea un nuevo hilo o thread mediante el método `new()`, no implica que el hilo ya se pueda ejecutar.

Para que el hilo se pueda ejecutar, debe estar en el estado `Ejecutable`, y para conseguir ese estado es necesario iniciar o arrancar el hilo mediante el método `start()` de la clase `Thread`.

En los ejemplos anteriores, recuerda que teníamos el código `hilo1.start()`, que precisamente se encargaba de iniciar el hilo representado por el objeto `Thread` `hilo1`.

En realidad el método `start()` **realiza las siguientes tareas**:

* Crea los recursos del sistema necesarios para ejecutar el hilo.

* Se encarga de llamar a su método `run()` y lo ejecuta como un subproceso nuevo e independiente.

Es por esto último que cuando se invoca a `start()` se suele decir que el hilo está `corriendo` (`running`), pero recuerda que esto no significa que el hilo esté ejecutándose en todo momento, ya que un hilo `Ejecutable` puede estar `Preparado` o `Ejecutándose` según tenga o no asignado tiempo de procesamiento.

Algunas **consideraciones importantes** que debes tener en cuenta son las siguientes:

* **Puedes invocar directamente al método `run()`**, por ejemplo poner `hilo1.run()` y se ejecutará el código asociado a `run()` dentro del hilo actual (como cualquier otro método), **pero no comenzará un nuevo hilo como subproceso independiente**.

* Una vez que se ha llamado **al método `start()` de un hilo, no puedes volver a realizar otra llamada** al mismo método. Si lo haces, obtendrás una excepción `IllegalThreadStateException`.

* El orden en el que inicies los hilos mediante `start()` no influye en el orden de ejecución de los mismos, lo que pone de manifiesto que **el orden de ejecución de los hilos es no-determinístico** (no se conoce la secuencia en la que serán ejecutadas la instrucciones del programa).

En el siguiente recurso didáctico puedes ver un programa que define dos hilos, construidos cada uno de ellos por los procedimientos vistos anteriormente. Cada hilo imprime una palabra 5 veces. Observa que si ejecutas varias veces el programa, el orden de ejecución de los hilos no es siempre el mismo y que no influye en absoluto el orden en el que se inician con `start()` (el orden de ejecución de los hilos es no-determinístico).

![Captura flash](doc/PSP02_CONT_R27_EjecutarVariosHilos/captura.png) [SWF](doc/PSP02_CONT_R27_EjecutarVariosHilos/flash.swf) [HTML](doc/PSP02_CONT_R27_EjecutarVariosHilosr/flash.html) [REPO]()

Nota: puede que tengas que aumentar el número de iteraciones (número de palabras que imprime cada hilo) para apreciar las observaciones indicadas anteriormente.

## Detener temporalmente un hilo

¿Qué significa que un hilo se ha detenido temporalmente? Significa que el hilo ha pasado al estado "No Ejecutable".

Y ¿cómo puede pasar un hilo al estado "No Ejecutable"? **Un hilo pasará al estado `No Ejecutable` o `Detenido` por alguna de estas circunstancias**:

* **El hilo se ha dormido**. Se ha invocado al método `sleep()` de la clase `Thread`, indicando el tiempo que el hilo permanecerá deteniendo. Transcurrido ese tiempo, el hilo se vuelve `Ejecutable`, en concreto pasa a `Preparado`.

* **El hilo está esperando**. El hilo ha detenido su ejecución mediante la llamada al método `wait()`, y no se reanudará, pasará a `Ejecutable` (en concreto `Preparado`) hasta que se produzca una llamada al método `notify()` o `notifyAll()` por otro hilo. Estudiaremos detalladamente estos métodos de la clase `Object` cuando veamos la sincronización y comunicación de hilos.

* **El hilo se ha bloqueado**. El hilo está pendiente de que finalice una operación de E/S en algún dispositivo, o a la espera de algún otro tipo de recurso; ha sido bloqueado por el sistema operativo. Cuando finaliza el bloqueo, vuelve al estado `Ejecutable`, en concreto `Preparado`.
En la siguiente imagen puedes ver un esquema con los diferentes métodos que hacen que un hilo pase al estado `No Ejecutable`, así cómo los que permiten salir de ese estado y volver al estado `Ejecutable`.

![Ilustración detener temporalmente un hilo](img/detenerTemporalmenteUnHilo.png)

El método `suspend()` (actualmente en desuso o deprecated) también permite detener temporalmente un hilo, y en ese caso se reanudaría mediante el método `resume()` (también en desuso). No debes utilizar estos métodos, de la clase `Thread` ya que no son seguros y provocan muchos problemas. Te lo indicamos simplemente porque puede que encuentres programas que aún utilizan estos métodos.

## Finalizar un hilo

La forma natural de que muera o finalice un hilo es cuando termina de ejecutarse su método `run()`, pasando al estado `Muerto`.

**Una vez que el hilo ha muerto, no lo puedes iniciar otra vez con `start()`**. Si en tu programa deseas realizar otra vez el trabajo desempeñado por el hilo, tendrás que:

1. Crear un nuevo hilo con `new()`. 
2. Iniciar el nuevo hilo con `start()`

Y ¿hay alguna forma de comprobar si un hilo no ha muerto?

No exactamente, pero puedes utilizar el método `isAlive()` de la clase `Thread` para comprobar si un hilo está vivo o no. **Un hilo se considera que está vivo (`alive`) desde la llamada a su método `start()` hasta su muerte**. `isAlive()` devuelve verdadero (true) o falso (false), según que el hilo esté vivo o no.

Cuando el método isAlive() devuelve:

* `False`: sabemos que estamos ante un nuevo hilo recién "creado" o ante un hilo "muerto".
* `True`: sabemos que el hilo se encuentra en estado "ejecutable" o "no ejecutable".

El método `stop()` de la clase `Thread` (actualmente en desuso) también finaliza un hilo, pero es poco seguro. No debes utilizarlo. Te lo indicamos aquí simplemente porque puede que encuentres programas utilizando este método.

En el siguiente ejemplo, te proporcionamos un programa cuyo hilo principal lanza un hilo secundario que realiza una cuenta atrás desde 10 hasta 1. Desde el hilo principal se verificará la muerte del hilo secundario mediante la función `isAlive()`. Además mediante el método `getState()` de la clase `Thread` vamos obteniendo el estado del hilo secundario. Se usa también el método `Thread.join()` que espera hasta que el hilo muere.

**Clase `Hilo_Auxiliar.java`**:

```java
public class Hilo_Auxiliar extends Thread{
//código del hilo
  @Override
  public void run(){
    for(int i=10;i>=1;i--)
      System.out.print(i+",");
  }
}
```

**Clase `Main.java`**:

```java
public class Main {
    public static void main(String[] args) {
        Hilo_Auxiliar hilo1 = new Hilo_Auxiliar();
        //Crea un nuevo hilo. El hilo está en estado Nuevo (new)

        System.out.println("Hilo Auxiliar Nuevo: Estado=" + hilo1.getState()
                + ",¿Vivo? isAlive()=" + hilo1.isAlive());
        //Obtenemos el estado del thread hilo1 y si está vivo o no

        hilo1.start();
        //Inicia el thread hilo1 y pasa al estado Ejecutable

        System.out.println("Hilo Auxiliar Iniciado: Estado="
                + hilo1.getState()
                + ",¿Vivo? isAlive()=" + hilo1.isAlive() + "\n");
       
        try {
            hilo1.join();
            //espera a que el thread hilo1 muera
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        System.out.println("\n Hilo Auxiliar Muerto: Estado="
                + hilo1.getState()
                + ",¿Vivo? isAlive()=" + hilo1.isAlive());
    }
}
```

## Ejemplo. Dormir un hilo con sleep

¿Por qué puede interesar dormir un hilo? Pueden ser diferentes las razones que nos lleven a dormir un hilo durante unos instantes. En este apartado veremos un ejemplo en el que si no durmiéramos unos instantes al hilo que realiza un cálculo, no le daría tiempo al hilo que dibuja el resultado a presentarlo en pantalla.

**¿Cómo funciona el método `sleep()`?**

El método `sleep()` de la clase `Thread` recibe como argumento el tiempo que deseamos dormir el hilo que lo invoca. Cuando transcurre el tiempo especificado, el hilo vuelve a estar "Ejecutable" ("Preparado") para continuar ejecutándose. Hay dos formas de llamar a este método.

1. La primera le pasa como argumento un entero (positivo) que representa milisegundos:

```java
 sleep(long milisegundos)
```

2. La segunda le agrega un segundo argumento entero (esta vez, entre 1 y 999999), que representa un tiempo extra en nanosegundos que se sumará al primer argumento:

```java
sleep(long milisegundos, int nanosegundos)
```

Cualquier llamada a `sleep()` puede provocar una excepción, que el compilador de Java nos obliga a controlar ineludiblemente mediante un bloque `try-catch`.

El siguiente recurso didáctico ilustra la necesidad de dormir un hilo en una aplicación que muestra como avanza un marcador gráfico desde 0 hasta 20. Podrás comprobar que si no utilizamos un hilo auxiliar y lo dormimos, no podremos apreciar como se va incrementando el marcador.

![Captura flash](doc/PSP02_CONT_R32_DormirHilo_marcador/captura.png) [SWF](doc/PSP02_CONT_R32_DormirHilo_marcador/flash.swf) [HTML](doc/PSP02_CONT_R32_DormirHilo_marcador/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-domirhiloconmarcadorgrafico/src/master/)

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)