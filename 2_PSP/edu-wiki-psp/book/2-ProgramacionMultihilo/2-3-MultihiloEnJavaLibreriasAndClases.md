# Multihilo en Java. Librerías y clases

Tal y como ha comentado Juan, Java da soporte al concepto de hilo desde el propio lenguaje con algunas clases e   interfaces definidas en el paquete java.lang y con   métodos específicos para la manipulación de hilos en la clase `Object`.

A partir de la versión `JavaSE 5.0`, se incluye el paquete `java.util.concurrent` con nuevas utilidades para desarrollar aplicaciones multihilo e incluso aplicaciones con un alto nivel de concurrencia.

## Utilidades de concurrencia del paquete `java.lang`

Dentro del paquete `java.lang` disponemos de una interfaz y las siguientes clases para trabajar con hilos:

* **Clase** `Thread`. Es la clase responsable de producir hilos funcionales para otras clases y proporciona gran parte de los métodos utilizados para su gestión.

* **Interfaz** `Runnable`. Proporciona la capacidad de añadir la funcionalidad de hilo a una clase simplemente implementando la interfaz, en lugar de derivándola de la clase `Thread`.

* **Clase** `ThreadDeath`. Es una clase de error, deriva de la clase Error, y proporciona medios para manejar y notificar errores.

* **Clase** `ThreadGroup`. Esta clase se utiliza para manejar un grupo de hilos de modo conjunto, de manera que se pueda controlar su ejecución de forma eficiente.

* **Clase** `Object`. Esta clase no es estrictamente de apoyo a los hilos, pero proporciona unos cuantos métodos cruciales dentro de la arquitectura multihilo de Java. Estos métodos son `wait()`, `notify()` y `notifyAll()`.

En el siguiente enlace tienes una tabla resumida de la clase `Thread`, en español, que incluye los métodos más comunes para gestionar y controlar hilos.

## Utilidades de concurrencia del paquete `java.util.concurrent`

El paquete `java.util.concurrent` incluye una serie de clases que facilitan enormemente el desarrollo de aplicaciones multihilo y aplicaciones complejas, ya que están concebidas para utilizarse como bloques de diseño.

Concretamente estas utilidades están dentro de los siguientes paquetes:

* `java.util.concurrent`. En este paquete están definidos los siguientes elementos:

	* **Clases de sincronización**. `Semaphore`, `CountDownLatch`, `CyclicBarrier` y `Exchanger`.

	* **Interfaces para separar la lógica de la ejecución**, como por ejemplo `Executor`, `ExecutorService`, `Callable` y `Future`.

	* **Interfaces para gestionar colas de hilos**. `BlockingQueque`, `LinkedBlokingQueque`, `ArrayBlockingQueque`, `SynchronousQueque`, `PriorityBlockingQueque` y `DelayQueque`.

* `java.util.concurrent.atomic`. Incluye un conjunto de clases para ser usadas como variables atómicas en aplicaciones multihilo y con diferentes tipos de dato, por ejemplo `AtomicInteger` y `AtomicLong`.

*  `java.util.concurrent.locks`. Define una serie de clases como uso alternativo a la cláusula `sinchronized`. En este paquete se encuentran algunas interfaces como por ejemplo `Lock`, `ReadWriteLock`.
 
A lo largo de esta unidad estudiaremos las clases e interfaces más importantes de este paquete.



> Cita para pensar:
> 
> "*Vale más saber alguna cosa de todo, que saberlo todo de una sola cosa*" (Blaise Pascal)

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)