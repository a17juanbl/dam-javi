# Bienvenido

En esta wiki tratará de recopilar y agregar contenidos dispersos por diferentes fuentes.

Revisa las licencias y derechos reservados de propiedad intelectual © cuando así se especifique.

## Colaborar

Lo que pretende esta Wiki es agregar y sintetizar ciertos contenidos, resumiéndolos y estructurándolos para poder ver ciertos conceptos desde un punto de vista o perspectiva diferente.

Esta wiki una la sintaxis [Markdown](http://daringfireball.net/projects/markdown/). El [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) muestra cómo varios elementos son dibujados o renderizados según esta sintaxis. La [documentación de Bitbucket](https://confluence.atlassian.com/x/FA4zDQ) muestra más información sobre como usar la wiki.

La wiki en sí misma es un repositorio git, lo que significa que tú puedes clonarlo, editarlo de forma local y desconectada, añadir imágenes o cualquier otro tipo de fichero, y realizar un push para contribuir en él, y también para verlo online de forma inmediata.

Puedes clonar el repo de la siguiente manera:

```
$ git clone <url>
```

Las páginas de la Wiki son ficheros normales, con la extensión `.md`. Puedes editarlos localmente tanto como crear unos nuevos.

*No se autorizará la modificación de contenidos bajo licencia no libre cuando esta sea reseñada. ©*

## Contenidos

Estos son los contenidos principales del repo:
